import React from "react";
import renderer from "react-test-renderer"
import { BrowserRouter } from "react-router-dom";
import About from "../pages/About";
import Splash from "../pages/Home";
import Ingredients from "../pages/Ingredients"
import Recipes from "../pages/Recipes"
import Restaurants from "../pages/Restaurants"
import Navbar from "../components/Navigation";
import IngredientsCard from "../components/cards/IngredientsCard"
import RecipesCard from "../components/cards/RecipesCard"
import RestaurantsCard from "../components/cards/RestaurantsCard";
// Code format referenced from https://gitlab.com/joshuachen1/cs373-idb/-/blob/main/front-end/src/__tests__/JestTests.test.js

// 1. Test Navbar for Crash
it("renders navbar without apparent crash", () => {
  <BrowserRouter>
      render(<Navbar />);
      expect(screen.getByText("Home")).toBeInTheDocument();
  </BrowserRouter>;
})


// 2. Test for Name render on About page
it("renders About without crashing", () => {
    <BrowserRouter>
        render(<About />)
        expect(screen.getByText('Sumit')).toBeInTheDocument();
    </BrowserRouter>;
})

jest.mock("../pics/splash_background.jpg", () => "test-file-stub");

// 3. Test for website name display on Splash page
it("renders NutriNet on Home", () => {
  <BrowserRouter>
    render(<Splash />)
    expect(screen.getByText('NutriNet')).toBeInTheDocument();
  </BrowserRouter>;
})

// 4. Test for Ingredients
it("renders Ingredients Page", () => {
  <BrowserRouter>
    render(<Ingredients />)
    expect(screen.getByText('Ingredients Search')).toBeInTheDocument();
  </BrowserRouter>;
})

// 5. Test for Recipes
it("renders Recipes Page", () => {
  <BrowserRouter>
    render(<Recipes />)
    expect(screen.getByText('Recipes Search')).toBeInTheDocument();
  </BrowserRouter>;
})

// 6. Test for Restaurants
it("renders Restaurants Page", () => {
  <BrowserRouter>
    render(<Restaurants />)
    expect(screen.getByText('Restaurants Search')).toBeInTheDocument();
  </BrowserRouter>;
})

// 7. Test for Ingredient Card
it("renders Ingredient Card", () => {
  const ing =  {
    "calories": 2.63,
    "cholesterol": 0.0,
    "fat": 0.0869,
    "id": 1,
    "image_src": "https://www.edamam.com/food-img/c3f/c3f96d47d334b92f0120ff0b3a512ec3.jpg",
    "labels": [
      "FAT_FREE",
      "LOW_SUGAR",
      "KETO_FRIENDLY",
      "VEGAN",
      "VEGETARIAN",
      "PESCATARIAN",
      "PALEO",
      "SPECIFIC_CARBS",
      "DAIRY_FREE",
      "GLUTEN_FREE",
      "WHEAT_FREE",
      "EGG_FREE",
      "MILK_FREE",
      "PEANUT_FREE",
      "TREE_NUT_FREE",
      "SOY_FREE",
      "FISH_FREE",
      "SHELLFISH_FREE",
      "PORK_FREE",
      "RED_MEAT_FREE",
      "CRUSTACEAN_FREE",
      "CELERY_FREE",
      "MUSTARD_FREE",
      "SESAME_FREE",
      "LUPINE_FREE",
      "MOLLUSK_FREE",
      "ALCOHOL_FREE",
      "NO_OIL_ADDED",
      "NO_SUGAR_ADDED",
      "KOSHER"
    ],
    "name": "5 spice powder",
    "protein": 0.0609,
    "sodium": 0.77,
    "weight": 1.0
  }

  const id = 1
  const comp = renderer.create(<IngredientsCard key = {id} ingredient = {ing} />)
  let t = comp.toJSON();
  expect(t).toMatchSnapshot();
})

// 8. Test for Recipe Card
it("renders Recipe Card", () => {
  const rec =  {
    "cuisines": [
      "African"
    ],
    "diets": [
      "gluten free",
      "dairy free"
    ],
    "id": 1,
    "img_src": "https://spoonacular.com/recipeImages/716298-556x370.jpg",
    "ingredients": [
      "bay leaves",
      "curry powder",
      "garlic",
      "diced ham",
      "onion",
      "black pepper",
      "rice",
      "italian tomato",
      "table salt",
      "habanero chili",
      "thyme",
      "tomato puree",
      "vegetable oil",
      "water"
    ],
    "instructions": [
      "*Wash rice by rubbing the rice between your palms in a bowl of water and draining the water till clear.Blend tomatoes, pepper and garlic and bring to boil till the excess water dries up.Chop Onions",
      "Heat up vegetable oil and pour in chopped onions and fry.",
      "Pour in the can of tomato puree and fry.",
      "Pour in blended tomato and pepper mix into the pot and stir in.",
      "Pour in salt, dry pepper, curry, thyme, bay leaves and maggi cubes.Allow it to simmer on low heat for 3 minutes.Reduce the heat to the lowest level and pour in the washed rice.",
      "Pour in the water and stir and leave on low heat for 20 minutes or till the rice is soft.Tip: To get the party rice flavor, increase the heat on the rice and burn the bottom of the pot with the pot covered and stir the rice after 3 minutes of burning.Stir the rice and serve with any protein of your choice.\u00a0 // <![CDATA[(adsbygoogle = window.adsbygoogle || []).push({});// ]]&gt;A video I shared on Instagram recently"
    ],
    "name": "How to Make Party Jollof Rice",
    "prep_time": 45,
    "recipe_src": "http://www.afrolems.com/2012/11/23/how-to-make-party-jollof-rice/",
    "servings": 3,
    "summary": "Need a <b>gluten free and dairy free hor d'oeuvre</b>? How to Make Party Jollof Rice could be an excellent recipe to try. This recipe makes 3 servings with <b>508 calories</b>, <b>11g of protein</b>, and <b>2g of fat</b> each. For <b>$1.25 per serving</b>, this recipe <b>covers 17%</b> of your daily requirements of vitamins and minerals. This recipe from Afrolems requires salt, scotch bonnet peppers, garlic, and can of tomato puree. 347 people found this recipe to be delicious and satisfying. It is an <b>affordable</b> recipe for fans of African food. From preparation to the plate, this recipe takes approximately <b>45 minutes</b>. With a spoonacular <b>score of 87%</b>, this dish is tremendous. If you like this recipe, you might also like recipes such as <a href=\"https://spoonacular.com/recipes/vegan-jollof-rice-1340013\">Vegan Jollof Rice</a>, <a href=\"https://spoonacular.com/recipes/one-pot-jollof-rice-1417893\">One-Pot Jollof Rice</a>, and <a href=\"https://spoonacular.com/recipes/vegan-jollof-rice-875497\">Vegan Jollof Rice</a>."
  }

  const id = 1
  const comp = renderer.create(<RecipesCard key = {id} recipe = {rec} />)
  let t = comp.toJSON();
  expect(t).toMatchSnapshot();
})

// 9. Test for Restaurant Card
it("renders Restaurant Card", () => {
  const res =  {
    "address": {
      "city": "Austin",
      "country": "US",
      "lat": 30.26659,
      "latitude": 30.26659,
      "lon": -97.74003,
      "longitude": -97.74003,
      "state": "TX",
      "street_addr": "318 East 5th Street",
      "street_addr_2": "",
      "zipcode": "78701"
    },
    "cuisines": [
      "African"
    ],
    "food_pic": "https://d1ralsognjng37.cloudfront.net/e662340d-ab14-4ce8-a128-5cd1bb57072d.jpeg",
    "fp_delivery": true,
    "fp_pickup": true,
    "id": 1,
    "logo_pic": "https://cdn-icons-png.flaticon.com/512/948/948036.png",
    "name": "Tchiadai",
    "operating_hours": {
      "Friday": "08:00PM - 03:00AM",
      "Monday": "08:00PM - 03:00AM",
      "Saturday": "08:00PM - 03:00AM",
      "Sunday": "08:00PM - 03:00AM",
      "Thursday": "08:00PM - 03:00AM",
      "Tuesday": "08:00PM - 03:00AM",
      "Wednesday": "08:00PM - 03:00AM"
    },
    "phone": "17372024886",
    "pricepoint": 0,
    "rating": 0.0,
    "rating_count": 0,
    "store_pic": "https://cdn-icons-png.flaticon.com/512/869/869636.png",
    "tp_delivery": true
  }

  const id = 1
  const comp = renderer.create(<RestaurantsCard key = {id} restaurant = {res} />)
  let t = comp.toJSON();
  expect(t).toMatchSnapshot();
})

// 10. Test for Restaurants Sort By
it("checks for Restaurant Sort", () => {
  <BrowserRouter>
    render(<Restaurants />)
    expect(screen.getByText('Sort By')).toBeInTheDocument();
  </BrowserRouter>;
})

// 11. Test for Recipes Sort By
it("checks for Recipes Sort", () => {
  <BrowserRouter>
    render(<Recipes />)
    expect(screen.getByText('Sort By')).toBeInTheDocument();
  </BrowserRouter>;
})

// 12. Test for Ingredients Sort By
it("checks for Ingredients Sort", () => {
  <BrowserRouter>
    render(<Ingredients />)
    expect(screen.getByText('Sort By')).toBeInTheDocument();
  </BrowserRouter>;
})

// 13. Test for Restaurants Search
it("checks for Restaurants Search", () => {
  <BrowserRouter>
    render(<Restaurants />)
    expect(screen.getByPlaceholderText('Search')).toBeInTheDocument();
  </BrowserRouter>;
})

// 14. Test for Recipes Search
it("checks for Recipes Search", () => {
  <BrowserRouter>
    render(<Recipes />)
    expect(screen.getByPlaceholderText('Search')).toBeInTheDocument();
  </BrowserRouter>;
})

// 15. Test for Ingredients Search
it("checks for Ingredients Search", () => {
  <BrowserRouter>
    render(<Ingredients />)
    expect(screen.getByPlaceholderText('Search')).toBeInTheDocument();
  </BrowserRouter>;
})