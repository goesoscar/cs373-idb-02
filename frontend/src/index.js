import React from 'react';
import ReactDOM from 'react-dom/client';
import { BrowserRouter as Router, Routes, Route } from 'react-router-dom';
import Navigation from './components/Navigation';

import Home from './pages/Home';
import About from './pages/About';
import Ingredients from './pages/Ingredients';
import IngredientDetail from './pages/IngredientDetail';
import RecipeDetail from './pages/RecipeDetail';
import Recipes from './pages/Recipes';
import Restaurants from './pages/Restaurants';
import RestaurantDetail from './pages/RestaurantDetail';
import ProviderVisualizations from './pages/visualizations/ProviderVisualizations';
import Visualizations from './pages/visualizations/Visualizations';
import Search from './pages/Search';

// Importing the Bootstrap CSS
import 'bootstrap/dist/css/bootstrap.min.css';

const root = ReactDOM.createRoot(document.getElementById('root'));
root.render(
  <Router>
    <Navigation />
    <Routes>
      <Route path="/" exact element={<Home />} />
      <Route path="/about" element={<About />} />
      <Route path="/ingredients" element={<Ingredients />} />
      <Route path="/recipes" element={<Recipes />} />
      <Route path="/restaurants" element={<Restaurants />} />
      <Route path="/ingredients/:id" element={<IngredientDetail/>} />
      <Route path="/recipes/:id" element={<RecipeDetail/>} />
      <Route path="/restaurants/:id" element={<RestaurantDetail/>} />
      <Route path="/visualizations" element={<Visualizations />} />
      <Route path="/providervisualizations" element={<ProviderVisualizations />} />
      <Route path="/search" element={<Search />} />
    </Routes>
  </Router>
);