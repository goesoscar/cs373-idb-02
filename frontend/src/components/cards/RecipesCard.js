import React from "react";
import Card from "react-bootstrap/Card";
import Button from "react-bootstrap/Button";
import Container from "react-bootstrap/Container";
import Row from "react-bootstrap/Row";
import Col from "react-bootstrap/Col";
import "../../styles/ResultsCards.css";
import Highlighter from "react-highlight-words";

/* Card structure loosely influenced by GeoJobs */
const RecipesCard = ({key, recipe, search}) => {
    function highlightSearch(text) {
        if (search != null) {
          return (
            <Highlighter
              searchWords={String(search).split(" ")}
              autoEscape={true}
              textToHighlight={text}
            />
          );
        }
        return text;
    }

    return (
        <Card className="results-card">
            <Card.Img src={recipe.img_src} className="results-card-img"/>
            <Card.Body>
                <Card.Title className="results-card-title">{highlightSearch(recipe.name)}</Card.Title>
                <Card.Text >
                    <Container >
                        <Row>
                            <Col>
                            <ul className="results-card-list">
                                <li><strong>Prep Time:</strong> {recipe.prep_time + " mins"}</li>
                                <li><strong>Servings:</strong> {recipe.servings}</li>
                                <li><strong>Cuisine Nationality:</strong> {recipe.cuisines.toString()}</li>
                                <li><strong>Diets:</strong> {
                                  recipe.diets && recipe.diets.length > 0 ?
                                  recipe.diets.map((diet) =>
                                    diet.replace(/_/g, " ").toLowerCase().replace(/\b\w/g, (match) => match.toUpperCase())
                                  ).join(", ") :
                                  "Not Specified"
                                }</li>
                                <li><strong>Featured Ingredient:</strong> {recipe.ingredients[0]}</li>
                            </ul>
                            </Col>
                        </Row>
                    </Container>
                </Card.Text>
                <Button href={`/recipes/${recipe.id}`} variant="primary" size="sm" className="btn-results-card">View Details</Button>
            </Card.Body>
        </Card>
    );
};

export default RecipesCard;
