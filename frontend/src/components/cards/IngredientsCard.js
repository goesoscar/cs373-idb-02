import React from "react";
import Card from "react-bootstrap/Card";
import Button from "react-bootstrap/Button";
import Container from "react-bootstrap/Container";
import Row from "react-bootstrap/Row";
import Col from "react-bootstrap/Col";
import "../../styles/ResultsCards.css";
import Highlighter from "react-highlight-words";

/* Card structure loosely influenced by GeoJobs */
const IngredientsCard = ({key, ingredient, search}) => {
  function highlightSearch(text) {
    if (search != null) {
      return (
        <Highlighter
          searchWords={String(search).split(" ")}
          autoEscape={true}
          textToHighlight={text}
        />
      );
    }
    return text;
  }

  return (
    <Card className="results-card">
      <Card.Img src={ingredient.image_src} className="results-card-img" />
      <Card.Body>
        <Card.Title className="results-card-title">{highlightSearch(ingredient.name)}</Card.Title>
        <Container>
          <Row>
            <Col>
              <ul className="results-card-list">
                <li><strong>Calories:</strong> {ingredient.calories}cal</li>
                <li><strong>Cholesterol:</strong> {ingredient.cholesterol}mg</li>
                <li><strong>Fat:</strong> {ingredient.fat}g</li>
                <li><strong>Protein:</strong> {ingredient.protein}g</li>
                <li><strong>Sodium:</strong> {ingredient.sodium}mg</li>
              </ul>
            </Col>
          </Row>
        </Container>
        <Button href={`/ingredients/${ingredient.id}`} variant="primary" size="sm" className="btn-results-card">
            View Details
        </Button>
      </Card.Body>
    </Card>
  );
};

export default IngredientsCard;
