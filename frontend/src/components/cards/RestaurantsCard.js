import React from "react";
import Card from "react-bootstrap/Card";
import Button from "react-bootstrap/Button";
import Container from "react-bootstrap/Container";
import Row from "react-bootstrap/Row";
import Col from "react-bootstrap/Col";
import "../../styles/ResultsCards.css";
import Highlighter from "react-highlight-words";

/* Card structure loosely influenced by GeoJobs */
const RestaurantsCard = ({ key, restaurant, search }) => {
    function highlightSearch(text) {
        if (search != null) {
          return (
            <Highlighter
              searchWords={String(search).split(" ")}
              autoEscape={true}
              textToHighlight={text}
            />
          );
        }
        return text;
    }
    return (
        <Card className="results-card">
            <Card.Img src={restaurant.logo_pic} className="results-card-img"/>
            <Card.Body>
                <Card.Title className="results-card-title">{highlightSearch(restaurant.name)}</Card.Title>
                <Card.Text >
                    <Container >
                        <Row>
                            <Col>
                              <ul className="results-card-list">
                                <li>Type of Food: {restaurant.cuisines?.join(', ')}</li>
                                <li><strong>Rating:</strong> {restaurant.rating_count === 0 ? ('No Reviews') : ( <>{restaurant.rating} / 5.0</> )} </li>
                                <li><strong>Price:</strong> {restaurant.pricepoint !== 0 ? '$'.repeat(restaurant.pricepoint) : '$'}</li>
                                <li><strong>Offers Pickup?:</strong> {restaurant.fp_pickup ? "Yes" : "No"}</li>
                                <li><strong>Offers Delivery?</strong> {restaurant.fp_delivery ? "Yes" : "No"}</li>
                              </ul>
                            </Col>
                        </Row>
                        
                    </Container>
                </Card.Text>
                <Button href={`/restaurants/${restaurant.id}`} variant="primary" size="sm" className="btn-results-card">View Details</Button>
            </Card.Body>
        </Card>
    );
};

export default RestaurantsCard;