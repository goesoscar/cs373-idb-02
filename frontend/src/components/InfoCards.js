import React from 'react'
import { Card, Button, Row, Col } from 'react-bootstrap';

const InfoCards = ({ title, items }) => {
  return (
    <div>
      <Row className="my-4">
        <h1>{title}</h1>
      </Row>
      <Row className="my-4">
        {items.map((item) => (
          <Col xs={12} md={6} lg={4} className="my-4" key={item.name}>
            <Card className="info-card" style={{ borderRadius: '20px', boxShadow: '0 0 20px rgba(0,0,0,.1)', transition: 'box-shadow .3s', ':hover': { boxShadow: '0 0 40px rgba(0,0,0,.2)' } }}>
              <div className="info-card-img-wrapper" style={{ overflow: 'hidden', borderRadius: '20px 20px 0 0' }}>
                <Card.Img variant="top" src={item.img_src} style={{ objectFit: 'contain', height: '200px' }} />
              </div>
              <Card.Body className="text-center" style={{ padding: '20px' }}>
                <Card.Title style={{ fontSize: '24px', fontWeight: 'bold', marginBottom: '10px' }}>{item.name}</Card.Title>
                <Card.Text style={{ fontSize: '16px', marginBottom: '20px' }}>{item.caption}</Card.Text>
                <Button variant="primary" href={item.link}>Learn More</Button>
              </Card.Body>
            </Card>
          </Col>
        ))}
      </Row>
    </div>
  );
}

export default InfoCards