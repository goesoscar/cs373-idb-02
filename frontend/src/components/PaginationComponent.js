import React, { useState } from 'react';
import Pagination from 'react-bootstrap/Pagination';
import Form from 'react-bootstrap/Form';
import Button from 'react-bootstrap/Button';
import "../styles/PaginationComponents.css";

/* Pagination structure inspired by:
 CityScoop: https://gitlab.com/yingcctx/cs373-idb/-/blob/main/front-end/src/pages/Models/Model2.js,
 GeoJobs: https://gitlab.com/sarthaksirotiya/cs373-idb/-/blob/main/front-end/src/views/Jobs.jsx,
 and HealthChecks: https://gitlab.com/ruolin111/cs373-idb/-/blob/main/frontend/src/Components/MyPagination/MyPagination.js 
 */
const PaginationComponent = ({ pageNumbers, currentPage, setCurrentPage, handleItemsPerPageChange, formValue}) => {
  const lastPage = pageNumbers.length;
  const startPage = currentPage > 3 ? currentPage - 2 : 2;
  const endPage = currentPage + 2 < lastPage ? currentPage + 2 : lastPage - 1;
  const [tempItemsPerPage, setTempItemsPerPage] = useState(formValue);

  const getPageButtons = () => {
    const buttons = [];

    // Perm link to first page
    buttons.push(
      <Pagination.Item className="item" active={currentPage === 1} onClick={() => setCurrentPage(1)}>
        1
      </Pagination.Item>
    );

    if (startPage > 2) {
      buttons.push(<Pagination.Ellipsis className="item" />);
    }

    // Start pushing no more than 5 buttons in the current range
    for (let i = startPage; i <= endPage; i++) {
      buttons.push(
        <Pagination.Item className="item" active={i === currentPage} onClick={() => setCurrentPage(i)}>
          {i}
        </Pagination.Item>
      );
    }

    if (endPage < lastPage - 1) {
      buttons.push(<Pagination.Ellipsis className="item" />);
    }

    // Perm link to last page number
    if (lastPage > 1) {
      buttons.push(
        <Pagination.Item className="item" active={currentPage === lastPage} onClick={() => setCurrentPage(lastPage)}>
          {lastPage}
        </Pagination.Item>
      );
    }

    return buttons;
  };

  return (
    <div className='pagination-container'>
      <Pagination className='pagination'>
        <Pagination.Prev className="item" disabled={currentPage === 1} onClick={() => setCurrentPage(currentPage - 1)} />
        {getPageButtons()}
        <Pagination.Next className="item" disabled={currentPage === lastPage} onClick={() => setCurrentPage(currentPage + 1)} />
      </Pagination>
      <div>
        <Form className='form-group'>
          <Form.Group controlId="formBasicEmail">
            <Form.Label>Items Per Page</Form.Label>
            <Form.Control type="number" placeholder="Enter # items" value={tempItemsPerPage} onChange={(e) => setTempItemsPerPage(e.target.value)} />
          </Form.Group>
            <Button variant="primary" type="submit" onClick={(e) => {
              e.preventDefault();
              handleItemsPerPageChange(tempItemsPerPage);}}>
              Submit
            </Button>
        </Form>
      </div>
    </div>
  );
};

export default PaginationComponent;
