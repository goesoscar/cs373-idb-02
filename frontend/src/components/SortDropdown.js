import React from 'react'
import { useState, useEffect } from 'react';
import Dropdown from 'react-bootstrap/Dropdown';

const SortDropdown = ({ options, handleDropdownChange }) => {

const [sortOption, setSortOption] = useState('');

  const handleSubmit = (eventKey) => {
    setSortOption(eventKey);
    // Perform the search operation here.
  };

    useEffect(() => {
        // if (sortOption) {
            handleDropdownChange(sortOption);
        // }
    }, [sortOption, handleDropdownChange]);

  return (
    <div>
      <p>Sort by</p>
      <Dropdown onSelect={handleSubmit}>
      <Dropdown.Toggle variant="success" id="dropdown-basic">
        {sortOption}
      </Dropdown.Toggle>

      <Dropdown.Menu>
        {options.map((option) => (
            <Dropdown.Item key={option} eventKey={option} value={option} href="#">
                {option ? option : 'None'}
            </Dropdown.Item>
        ))}
      </Dropdown.Menu>
    </Dropdown>

    </div>
   
  )
}

export default SortDropdown