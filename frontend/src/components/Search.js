import React, { useState } from 'react';
import Button from 'react-bootstrap/Button';
import Form from 'react-bootstrap/Form';
import FormControl from 'react-bootstrap/FormControl';

const Search = ({ handleSearchChange }) => {
  const [searchTerm, setSearchTerm] = useState('');

  const handleChange = (event) => {
    setSearchTerm(event.target.value);
  };

  const handleSubmit = (event) => {
    event.preventDefault();
    console.log('Search term:', searchTerm);
    handleSearchChange(searchTerm);
    // Perform the search operation here.
  };

  return (
    <div>
    <Form inline onSubmit={handleSubmit}>
            <FormControl
            type="text"
            placeholder="Search"
            className="mr-sm-2"
            value={searchTerm}
            onChange={handleChange}
        />
        <Button variant="outline-success" type="submit">
            Search
        </Button>
      
    </Form>
    </div>
  );
};

export default Search;