import React from 'react'
import Col from 'react-bootstrap/Col';
import Button from 'react-bootstrap/Button';
import Card from 'react-bootstrap/Card';
import Container from 'react-bootstrap/Container';
import Row from 'react-bootstrap/Row';

const Cards = ({ data }) => {
    
    const items = data.map((item, index) => {
        return (
        <Col key={index}>
            <Card style={{ width: '18rem' }}>
                <Card.Body>
                    <Card.Title>{item.title}</Card.Title>
                    <Card.Text>
                        
                    </Card.Text>
                    <Button variant="primary" href={item.link}>Learn More</Button>
                </Card.Body>
            </Card>
        </Col>
        );
    });

    return (
        <Container fluid>
            <Row>
                {items}
            </Row>
        </Container>
    );
};


export default Cards;