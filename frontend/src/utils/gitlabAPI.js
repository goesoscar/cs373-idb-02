import axios from 'axios';

export const getBranchNames = async () => {
  try {
    const response = await axios.get('https://gitlab.com/api/v4/projects/43437643/repository/branches');
    const branches = response.data.map((branch) => branch.name);
    return branches;
  } catch (error) {
    console.error(error);
  }
};


export const getBranchCommits = async () => {
  try {
    const pages = [1, 2, 3];
 
    // Create an array of promises to fetch data from both pages
    const promises = pages.map((page) => {
      return axios.get(
        `https://gitlab.com/api/v4/projects/43437643/repository/commits?ref_name=main&all=True&page=${page}&per_page=100`
      );
    });

    // Fetch data from both pages concurrently and combine the results
    const responses = await Promise.all(promises);
    const combinedData = responses.reduce((acc, response) => {
      return acc.concat(response.data);
    }, []);

    return combinedData;
  } catch (error) {
    console.error(error);
  }
};


export const getIssues = async () => {
    try {
        const response = await axios.get('https://gitlab.com/api/v4/projects/43437643/issues?scope=all&per_page=100');
        return response.data;
    } catch (error) {
        console.error(error);
    }
}




