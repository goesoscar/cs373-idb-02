import axios from 'axios';

const baseURL = 'https://api.nutrinet.me';

export const getSearch = async (model, search) => {
    try {
      const response = await axios.get(`${baseURL}/${model}?search=${search}`);
      console.log(response.data);
      return response.data;
    } catch (error) {
      console.error(error);
    }
  };


export const getIngredients = async (search, sort, page, perPage) => {
    try {
      const response = await axios.get(`${baseURL}/ingredients?search=${search}&sort=${sort}&page=${page}&perPage=${perPage}`);
      return response.data;
    } catch (error) {
      console.error(error);
    }
  };

export const getIngredientByID = async (id) => {
    try {
      const response = await axios.get(`${baseURL}/ingredients/${id}`);
      return response.data;
    } catch (error) {
      console.error(error);
    }
}

export const getRecipes = async (search, sort, page, perPage) => {
    try {
      const response = await axios.get(`${baseURL}/recipes?search=${search}&sort=${sort}&page=${page}&perPage=${perPage}`);
      return response.data;
    } catch (error) {
      console.error(error);
    }
  };

export const getRecipeByID = async (id) => {
    try {
      const response = await axios.get(`${baseURL}/recipes/${id}`);
      return response.data;
    } catch (error) {
      console.error(error);
    }
}

export const getRestaurants = async (search, sort, page, perPage) => {
    try {
      
      const response = await axios.get(`${baseURL}/restaurants?search=${search}&sort=${sort}&page=${page}&perPage=${perPage}`);
      return response.data;
    } catch (error) {
      console.error(error);
    }
  };

export const getRestaurantByID = async (id) => {
    try {
      const response = await axios.get(`${baseURL}/restaurants/${id}`);
      return response.data;
    } catch (error) {
      console.error(error);
    }
}