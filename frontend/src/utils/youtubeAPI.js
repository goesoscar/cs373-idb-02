import axios from 'axios';

const youtubeAPI = axios.create({
  baseURL: 'https://www.googleapis.com/youtube/v3',
  params: {
    part: 'snippet',
    type: 'video',
    videoDefinition: 'high',
    maxResults: 1,
    key: 'AIzaSyAadMwEOIYtR0yaNOgFwi6xIjHpZLX8-rw',
  },// AIzaSyBRqoBaoRXn8atFQlLVZZEdTGnx1UqXz2g
});

export const getRecipeVideo = async (recipeName) => {
  try {
    const response = await youtubeAPI.get('/search', {
      params: {
        q: `How To Make ${recipeName}`,
      },
    });
    const videoId = response.data.items[0].id.videoId;
    console.log(videoId);
    return `https://www.youtube.com/embed/${videoId}`;
  } catch (error) {
    console.error(error);
  }
};
