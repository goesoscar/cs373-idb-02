import React, { useState, useEffect } from 'react';
import { getRestaurants } from '../utils/nutriRestAPI';
import RestaurantsCard from '../components/cards/RestaurantsCard';
import Search from '../components/Search';
import SortDropdown from '../components/SortDropdown';
import PaginationComponent from '../components/PaginationComponent';

const Restaurants = () => {
  const TOTAL_RESTAURANTS = 1237;
  const sortOptions = ['', 'name', 'pricepoint', 'rating', 'rating_count'];
  const [restaurants, setRestaurants] = useState([]);
  const [currentPage, setCurrentPage] = useState(1);
  const [search, setSearch] = useState('');
  const [sort, setSort] = useState('');
  const [itemsPerPage, setItemsPerPage] = useState(100);
  const [pageNumbers, setPageNumbers] = useState(Array.from({ length: Math.ceil(TOTAL_RESTAURANTS / itemsPerPage) }, (_, i) => i + 1));
  
  const handlePageChange = (pageNum) => {
    setCurrentPage(pageNum);
  };

  const handleItemsPerPageChange = (e) => {
    const value = parseInt(e);
    setItemsPerPage(value);
    setPageNumbers(Array.from({ length: Math.ceil(TOTAL_RESTAURANTS / value) }, (_, i) => i + 1));
    setCurrentPage(1);
  };

  useEffect(() => {
    const fetchData = async () => {
      const result = await getRestaurants(search, sort, currentPage, itemsPerPage);
      setRestaurants(result.data);
    };
    fetchData();
  }, [search, sort, currentPage, itemsPerPage]);

  useEffect(() => {
    setPageNumbers(
      Array.from(
        { length: Math.ceil(TOTAL_RESTAURANTS / itemsPerPage) },
        (_, i) => i + 1
      )
    );
    setCurrentPage(1);
  }, [itemsPerPage]);
  

  return (
    <div style={{textAlign: 'center'}}>
      <h2>Restaurants Search</h2>
      <p>Sampling From {TOTAL_RESTAURANTS} Restaurants</p>
      <PaginationComponent
        pageNumbers={pageNumbers}
        currentPage={currentPage}
        setCurrentPage={handlePageChange}
        handleItemsPerPageChange={handleItemsPerPageChange}
        formValue={itemsPerPage}
        setFormValue={setItemsPerPage}
      />
      <Search handleSearchChange={setSearch} />
      <SortDropdown options={sortOptions} handleDropdownChange={setSort} />
      <div className="d-flex flex-wrap gap-4" style={{display: 'flex', justifyContent: 'center'}}>
        {restaurants.map((restaurant) => (
          <RestaurantsCard
            key={restaurant.id}
            restaurant={restaurant}
            search={search}
          />
        ))}
      </div>
      <PaginationComponent
        pageNumbers={pageNumbers}
        currentPage={currentPage}
        setCurrentPage={handlePageChange}
        handleItemsPerPageChange={handleItemsPerPageChange}
        formValue={itemsPerPage}
        setFormValue={setItemsPerPage}
      />
    </div>
  );
};

export default Restaurants;
