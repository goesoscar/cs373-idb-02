import React from 'react'
import axios from 'axios'
import { useState, useEffect } from 'react'
import { Chart as ChartJS, RadialLinearScale, ArcElement, Tooltip, Legend} from 'chart.js'
import { PolarArea } from 'react-chartjs-2'

/* API Structure inspired by GeoJobs, Lowball, and AustinEats */
/*  Mostly from Lowball though */
/* https://gitlab.com/mihikabirmiwal/cs373-idb/-/blob/develop/front-end/src/components/Visualizations/CulturePrepTimes.js */
/* Also from ChartJS Documentation */
/* https://react-chartjs-2.js.org/examples/polar-area-chart/ */

ChartJS.register(
  RadialLinearScale,
  ArcElement,
  Tooltip,
  Legend
);

var colorData = [];

const NutrinetRestaurants = () => {

  const [data, setData] = useState({})
  const URL = "https://api.nutrinet.me/restaurants?perPage=1237"

  useEffect(() => {
    const fetchData = async () => {
      let response = await axios.get(URL);
      setData(get_avgs(response.data.data));
    };
    fetchData().then(() => console.log("restaurant avgs loaded"));
  }, [URL])

  const graphData = {
    labels: data[0],
    datasets: [{
      data: data[1],
      backgroundColor: colorData[0],
      borderColor: colorData[1],
      borderWidth: 1,
    },],
  };
  const options = {
    responsive: true,
    plugins: {
      legend: {
        display: true,
      },
    },
  };

  return (
    <div><h3>Average Ratings For Each Cuisine Group of Restaurants</h3>
      <div style={{height: '750px', width: '750px'}}>
      <PolarArea data={graphData} options={options} />
      </div>
    </div>
  )
}

const get_avgs = (restaurants) => {
  var avgs = new Map();
  for (const rest of restaurants) {
    /* Only consider 0.0 scores with >0 num ratings */
    if (rest["rating_count"] !== 0) {
      var cuisines = rest["cuisines"];
      if (cuisines) {
        for (const c of cuisines) {
          if (avgs.has(c)) {
            avgs.set(c, [avgs.get(c)[0] + rest["rating"], avgs.get(c)[1] + 1])
          } else {
            avgs.set(c, [rest["rating"], 1]);
          }
        }
      }
    }
  }
  var ret_cols = [];
  ret_cols[0] = Array.from(avgs.keys());
  var averages = []
  // Get average rating for each cuisine by total ratings sum / # restaurants
  for (const entry of avgs) {
    averages.push(entry[1][0] / entry[1][1])
  }
  ret_cols[1] = averages;
  get_colors(ret_cols[0].length);
  return ret_cols;
}

const get_colors = (num_data) => {
  var colors = new Map();
  var r = 0;
  var g = 0;
  var b = 0;
  for (let i = 0; i < num_data; i++) {
    r = Math.floor(Math.random() * 200) + 50;
    g = Math.floor(Math.random() * 200) + 50;
    b = Math.floor(Math.random() * 200) + 50;
    colors.set('rgba(' + r + ', ' + g + ', ' + b + ', 0.8)', 'rgb(' + r + ', ' + g + ', ' + b + ')')
  }
  var ret_colors = [];
  ret_colors[0] = Array.from(colors.keys());
  ret_colors[1] = Array.from(colors.values());
  colorData = ret_colors;
  return ret_colors;
}

export default NutrinetRestaurants;
