import React from 'react'
import axios from 'axios'
import { useState, useEffect } from 'react'
import { Chart as ChartJS, ArcElement, Tooltip, Legend } from 'chart.js'
import { Pie } from 'react-chartjs-2'

/* API Structure inspired by GeoJobs, Lowball, and AustinEats */
/*  Mostly from Lowball though */
/* https://gitlab.com/mihikabirmiwal/cs373-idb/-/blob/develop/front-end/src/components/Visualizations/CulturePrepTimes.js */
/* Also from ChartJS Documentation */
/* https://react-chartjs-2.js.org/examples/vertical-bar-chart */

ChartJS.register(
  ArcElement,
  Tooltip,
  Legend
);

var colorData = [];

const NutrinetRecipes = () => {

  const [data, setData] = useState({})
  const URL = "https://api.nutrinet.me/recipes?perPage=421"

  useEffect(() => {
    const fetchData = async () => {
      let response = await axios.get(URL);
      setData(get_cuisines(response.data.data));
    };
    fetchData().then(() => console.log("recipe cuisines loaded"));
  }, [URL])

  const graphData = {
    labels: data[0],
    datasets: [{
      data: data[1],
      backgroundColor: colorData[0],
      borderColor: colorData[1],
      borderWidth: 1
    },],
  };

  const options = {
    responsive: true,
    plugins: {
      legend: {
        display: true,
      },
    },
  };

  return (
    <div><h3>Number of Each Recipe Classified Under Each Cuisine Type</h3>
      <div style={{height: '750px', width: '750px'}}>
      <Pie data={graphData} options={options} />
      </div>
    </div>
  )
}

const get_cuisines = (recipes) => {
  var cuisineCounts = new Map();
  for (const rec of recipes) {
    var cuisines = rec["cuisines"];
    if (cuisines) {
      for (const c of cuisines) {
        if (cuisineCounts.has(c)) {
          cuisineCounts.set(c, cuisineCounts.get(c) + 1);
        } else {
          cuisineCounts.set(c, 1);
        }
      }
    }
  }
  var ret_cols = [];
  ret_cols[0] = Array.from(cuisineCounts.keys());
  ret_cols[1] = Array.from(cuisineCounts.values());
  get_colors(ret_cols[0].length);
  return ret_cols;
}

const get_colors = (num_data) => {
  var colors = new Map();
  var r = 0;
  var g = 0;
  var b = 0;
  for (let i = 0; i < num_data; i++) {
    r = Math.floor(Math.random() * 200) + 50;
    g = Math.floor(Math.random() * 200) + 50;
    b = Math.floor(Math.random() * 200) + 50;
    colors.set('rgba(' + r + ', ' + g + ', ' + b + ', 0.8)', 'rgb(' + r + ', ' + g + ', ' + b + ')')
  }
  var ret_colors = [];
  ret_colors[0] = Array.from(colors.keys());
  ret_colors[1] = Array.from(colors.values());
  colorData = ret_colors;
  return ret_colors;
}

export default NutrinetRecipes;
