import React from 'react'
import axios from 'axios'
import { useState, useEffect } from 'react'
import { Chart as ChartJS, CategoryScale, LinearScale, BarElement, Title, Tooltip, Legend } from 'chart.js'
import { Bar } from 'react-chartjs-2'

/* API Structure inspired by GeoJobs, Lowball, and AustinEats */
/*  Mostly from Lowball though */
/* https://gitlab.com/mihikabirmiwal/cs373-idb/-/blob/develop/front-end/src/components/Visualizations/CulturePrepTimes.js */
/* Also from ChartJS Documentation */
/* https://react-chartjs-2.js.org/examples/pie-chart */

// TODO the X axis does not display all of the categories
// but there is seemingly no way to widen it
// maybe we only do type1 instead of all three? idk
ChartJS.register(
  CategoryScale,
  LinearScale,
  BarElement,
  Title,
  Tooltip,
  Legend
);

var colorData = [];

const FlexerciseGyms = () => {

  const [data, setData] = useState({})
  const URL = "https://api.flexercise.social/gyms"

  useEffect(() => {
    const fetchData = async () => {
      let response = await axios.get(URL);
      setData(get_breakdowns(response.data))
    };
    fetchData().then(() => console.log("gyms loaded"));
  }, [URL])

  const graphData = {
    labels: data[0],
    datasets: [{
      data: data[1],
      backgroundColor: colorData[0],
      borderColor: colorData[1],
      borderWidth: 1
    },],
  };

  const options = {
    responsive: true,
    plugins: {
      title: {
        display: false,
      },
      legend: {
        display: false,
      },
    },
    scales: {
      y: {
        title: {
          display: true,
          text: '# of Establishments',
          font: {
            size: 12,
          }
        },
        beginAtZero: true
      }
    },
    legend: {
      display: false,
    },
  };

  return (
    <div><h3>Number of Each Type of Establishment</h3>
      <p><em>*Each establishment has at most three classifications</em></p>
      <p><em>This one takes several seconds to load due to the quantiy of data</em></p>
      <div style={{ height: '900px', width: '1200px'}}>
        <Bar data={graphData} options={options} />
      </div>
    </div>
  )
}

const get_breakdowns = (gyms) => {
  var counts = new Map();
  for (const g of gyms) {
    if (counts.has(g["type1"])) {
      counts.set(g["type1"], counts.get(g["type1"]) + 1);
    } else {
      counts.set(g["type1"], 1);
    }
    if (counts.has(g["type2"])) {
      counts.set(g["type2"], counts.get(g["type2"]) + 1);
    } else {
      counts.set(g["type2"], 1);
    }
    if (counts.has(g["type3"])) {
      counts.set(g["type3"], counts.get(g["type3"]) + 1);
    } else {
      counts.set(g["type3"], 1);
    }
  }
  var ret_cols = []
  ret_cols[0] = Array.from(counts.keys());
  ret_cols[1] = Array.from(counts.values());
  get_colors(ret_cols[0].length);
  return ret_cols;
}

const get_colors = (num_data) => {
  var colors = new Map();
  var r = 0;
  var g = 0;
  var b = 0;
  for (let i = 0; i < num_data; i++) {
    r = Math.floor(Math.random() * 200) + 50;
    g = Math.floor(Math.random() * 200) + 50;
    b = Math.floor(Math.random() * 200) + 50;
    colors.set('rgba(' + r + ', ' + g + ', ' + b + ', 0.8)', 'rgb(' + r + ', ' + g + ', ' + b + ')')
  }
  var ret_colors = [];
  ret_colors[0] = Array.from(colors.keys());
  ret_colors[1] = Array.from(colors.values());
  colorData = ret_colors;
  return ret_colors;
}

export default FlexerciseGyms;
