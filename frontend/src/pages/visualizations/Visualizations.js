import React from 'react'
import NutrinetIngredients from './NutrinetIngredients'
import NutrinetRecipes from './NutrinetRecipes'
import NutrinetRestaurants from './NutrinetRestaurants'

const Visualizations = () => {
  return (
    <div align="center">
        <h1>Visualizations</h1>
        <NutrinetIngredients />
        <NutrinetRecipes />
        <NutrinetRestaurants />
      </div>
        
  )
}

export default Visualizations;
