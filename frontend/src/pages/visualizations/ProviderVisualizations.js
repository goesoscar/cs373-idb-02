import React from 'react'
import FlexerciseExercises from './FlexerciseExercises'
import FlexerciseGroups from './FlexerciseGroups'
import FlexerciseGyms from './FlexerciseGyms'

const ProviderVisualizations = () => {
    return (
        <div align="center">
            <h1>Provider Visualizations</h1>
            <FlexerciseExercises />
            <FlexerciseGroups />
            <FlexerciseGyms />
        </div>
      )
}

export default ProviderVisualizations;
