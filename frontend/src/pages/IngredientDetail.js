import React, { useState, useEffect } from 'react'
import { useParams } from 'react-router-dom'
import { getIngredientByID } from '../utils/nutriRestAPI'
import Image from 'react-bootstrap/Image'
import Container from 'react-bootstrap/Container'
import Row from 'react-bootstrap/Row'
import Col from 'react-bootstrap/Col'
import { Chart as ChartJS, CategoryScale, LinearScale, BarElement, Title, Tooltip, Legend } from 'chart.js'
import { Bar } from 'react-chartjs-2'
import Button from 'react-bootstrap/esm/Button'
import { getRestaurants, getRecipes } from '../utils/nutriRestAPI'
import '../styles/IngredientCard.css'

ChartJS.register(
    CategoryScale,
    LinearScale,
    BarElement,
    Title,
    Tooltip,
    Legend
);

const IngredientDetail = () => {
    // const TOTAL_INGREDIENTS = 1000;
    const TOTAL_RECIPES = 421;
    const TOTAL_RESTAURANTS = 1237;
    const { id } = useParams()
    const [ingredient, setIngredient] = useState({})
    const [similarRetaurantID, setSimilarRestaurantID] = useState(Math.floor((Math.random() * TOTAL_RESTAURANTS) + 1))
    const [similarRecipeID, setSimilarRecipeID] = useState(Math.floor((Math.random() * TOTAL_RECIPES) + 1))
    
    useEffect(() => {
        const fetchData = async () => {
            const result = await getIngredientByID(id)
            setIngredient(result.data)
        };
        fetchData();
        // new stuff
        const searchTerm = ingredient.name + ' ' + ingredient.health_labels?.join(' ');
        const fetchSimilarRestaurant = async () => {
            const result = await getRestaurants(searchTerm, '', 1, 1);
            if (result.data.length > 0) {
                setSimilarRestaurantID(result.data[0].id);
            }
        }
        fetchSimilarRestaurant();

        const fetchSimilarRecipe = async () => {
            const result = await getRecipes(searchTerm, '', 1, 1);
            if (result.data.length > 0) {
                setSimilarRecipeID(result.data[0].id);
            }
        }
        fetchSimilarRecipe();
        
    }, [id, ingredient.name, ingredient.health_labels])
    let delayed;
    const data = {
        labels: ['Calories', 'Cholesterol', 'Fat', 'Protein', 'Sodium'],
        datasets: [{
            data: [ingredient.calories, ingredient.cholesterol, ingredient.fat, ingredient.protein, ingredient.sodium],
            backgroundColor: [
                '#51e263',
                '#048b4c',
                '#1cbb63',
                '#30cc5f',
                '#2ec05b'],
            borderColor: [
                '#0b9f54',
                '#0cb460',
                '#3cc45c',
                '#51e263',
                '#048b4c'],
            borderWidth: 1
        },
        ],
    };
    const options = {
        responsive: true,
        animation: {
            onComplete: () => {
                delayed = true;
            },
            delay: (context) => {
                let delay = 0;
                if (context.type === 'data' && context.mode === 'default' && !delayed) {
                    delay = context.dataIndex * 300 + context.datasetIndex * 100;
                }
                return delay;
            },
        },
        plugins: {
            title: {
                display: true,
                text: 'Nutrition Chart',
                font: {
                    size: 16,
                    weight: 'bold'
                }
            },
            legend: {
                display: false,
            },
        },
        scales: {
            y: {
                title: {
                    display: true,
                    text: '% Daily Value',
                    font: {
                      size: 12,
                    }
                },
                beginAtZero: true
            }
        },
        legend: {
            display: false,
        },
    };

    return (
        <Container>
            <Row className="my-4">
                <Col xs={12} md={6} className="d-flex justify-content-center">
                <div class="info">
                    <h1 style={{textTransform: 'capitalize'}}>{ingredient.name}</h1>
                    <div class="nutrition">
                        <li><strong>Calories:</strong> {ingredient.calories}cal</li>
                        <li><strong>Cholesterol:</strong> {ingredient.cholesterol}mg</li>
                        <li><strong>Fat:</strong> {ingredient.fat}g</li>
                        <li><strong>Protein:</strong> {ingredient.protein}g</li>
                        <li><strong>Sodium:</strong> {ingredient.sodium}g</li>
                        <li><strong>Weight:</strong> {ingredient.weight}g</li>
                    </div>
                </div> 
                </Col>
                <Col xs={12} md={6} className="d-flex justify-content-center">
                    <Image src={ingredient.image_src} className="ing-img" alt="My Image" />
                </Col>
            </Row>
            <Row>
                <div className="bar" style={{height: "400px", width:"100%", maxWidth: "100%"}}>
                    <Bar data={data} options={options} />
                </div>
            </Row>
            <Row>
                <h2 align="center">Health Labels</h2>
                <div className="d-flex flex-wrap gap-4" style={{display: 'flex', justifyContent: 'center'}}>
                {ingredient.labels && ingredient.labels.map((label) => {
                    /* Removes caps-lock and underscores */
                    const regexLabel = label.replace(/_/g, " ").toLowerCase().replace(/\b\w/g, (match) => match.toUpperCase());
                    return (
                        <li className="health-label">{ regexLabel }</li>
                    )
                }
                )}
                </div>
            </Row>
            
            <Row className="links">
        <Col className="d-flex justify-content-center">
          <Button className="btn-links" href={`https://www.nutrinet.me/restaurants/${similarRetaurantID}`}>Similar Restaurants</Button>
        </Col>
        <Col className="d-flex justify-content-center">
        <Button className="btn-links" href={`https://www.nutrinet.me/recipes/${similarRecipeID}`}>Similar Recipe</Button>
        </Col>
      </Row>
        </Container>
    )
}


export default IngredientDetail