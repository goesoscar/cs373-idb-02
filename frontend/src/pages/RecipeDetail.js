import React, { useState, useEffect } from 'react'
import { useParams } from 'react-router-dom'
import { getRecipeByID } from '../utils/nutriRestAPI'
import { getRecipeVideo } from '../utils/youtubeAPI'
import Image from 'react-bootstrap/Image'
import Container from 'react-bootstrap/Container'
import Row from 'react-bootstrap/Row'
import Col from 'react-bootstrap/Col'
import { Button } from 'react-bootstrap'
import { getIngredients, getRestaurants } from '../utils/nutriRestAPI'
import "../styles/RecipeCard.css"

const RecipeDetail = () => {
  const TOTAL_INGREDIENTS = 1000;
  const TOTAL_RESTAURANTS = 1237;
  const { id } = useParams()
  const [recipe, setRecipe] = useState({})
  const [videoUrl, setVideoUrl] = useState([])
  const [similarRetaurantID, setSimilarRestaurantID] = useState(Math.floor((Math.random() * TOTAL_RESTAURANTS) + 1))
  const [similarIngredientID, setSimilarIngredientID] = useState(Math.floor((Math.random() * TOTAL_INGREDIENTS) + 1))

  useEffect(() => {
    const fetchData = async () => {
      const result = await getRecipeByID(id)
      setRecipe(result.data)
    };
    fetchData();
    const searchTerm = recipe.name + ' ' + recipe.diets?.join(' ');
    const fetchSimilarRestaurant = async () => {
      const result = await getRestaurants(searchTerm, '', 1, 1);
      if (result.data.length > 0) {
        setSimilarRestaurantID(result.data[0].id);
      }
    }
    fetchSimilarRestaurant();

    const fetchSimilarIngredient = async () => {
      const result = await getIngredients(searchTerm, '', 1, 1);
      if (result.data.length > 0) {
        setSimilarIngredientID(result.data[0].id);
      }
    }
    fetchSimilarIngredient();
  }, [id, recipe.name, recipe.diets])

  useEffect(() => {
    const fetchVideo = async () => {
      setVideoUrl(await getRecipeVideo(recipe.name))
      //setVideoUrl("https://www.youtube.com/embed/dQw4w9WgXcQ")
    };
    fetchVideo();
    console.log("updating");
  },[recipe.name])

  return (
    <Container>
      <Row className="my-4">
        <Col xs={12} md={6} className="d-flex justify-content-center">
          <div class="info">
            <h1 style={{textTransform: 'capitalize'}}>{recipe.name}</h1>
            <div class="stats">
            <li><strong>Prep Time:</strong> {recipe.prep_time} minutes</li>
            <li><strong>Servings:</strong> {recipe.servings}</li>
            <li><strong>Cuisine Nationality:</strong> {recipe.cuisines}</li>
            <li><strong>Diets:</strong> {
              recipe.diets && recipe.diets.length > 0 ?
              recipe.diets.map((diet) =>
                diet.replace(/_/g, " ").toLowerCase().replace(/\b\w/g, (match) => match.toUpperCase())
              ).join(", ") :
              "Not Specified"
            }</li>
            </div>
          </div>
        </Col>
        <Col xs={12} md={6} className="d-flex justify-content-center">
          <Image src={recipe.img_src} className="rec-img" alt="My Image" />
        </Col>
      </Row> 
      <Row className="my-4">
        <Col>
        <p style={{textAlign: 'center'}}>{(recipe.summary)?.replace(/(<([^>]+)>)/ig, "")}</p>
        <h2>Ingredients</h2>
        {recipe.ingredients && recipe.ingredients.map((label) => {
          return (
            <Row>
              <Col>
                <li>{label.replace(/_/g, " ").toLowerCase().replace(/\b\w/g, (match) => match.toUpperCase())}</li>
              </Col>
            </Row>
          )
        })}
        </Col>
        <Col>
          {/* Video embed code taken directly from YouTube API Documentation */}
          {videoUrl && (
              <iframe
                width="560"
                height="315"
                src={videoUrl}
                title={`How To Make ${recipe.name}`}
                allowFullScreen
              />
            )}
        </Col>
      </Row>
      <Row>
        <Col> 
        <h2>Instructions</h2>
        <ol>
        {recipe.instructions && recipe.instructions.map((label) => {
            return (
                <Row>
                    <Col>
                        <li>{label}</li>
                    </Col>
                </Row>
            )
        }
        )}
        </ol>
        </Col>
      </Row>
      <Row className="links">
          <Col className="d-flex justify-content-center">
          <Button className="btn-links" href={recipe.recipe_src}>View Source of Recipe</Button>
          </Col>
        </Row>
        <Row className="links">
        <Col className="d-flex justify-content-center">
          <Button className="btn-links" href={`https://www.nutrinet.me/ingredients/${similarIngredientID}`}>Similar Ingredient</Button>
        </Col>
        <Col className="d-flex justify-content-center">
        <Button className="btn-links" href={`https://www.nutrinet.me/restaurants/${similarRetaurantID}`}>Similar Restaurant</Button>
        </Col>
      </Row>

    </Container>
)
}

export default RecipeDetail