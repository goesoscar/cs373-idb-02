import React from 'react';
import { Container, Row, Col, Button } from 'react-bootstrap';
import { Link } from 'react-router-dom';
import Background from '../pics/splash_background.jpg';

const Splash = () => (
  <div
    className="d-flex align-items-center"
    style={{
      backgroundImage: `url(${Background})`,
      backgroundSize: 'cover',
      height: '100vh',
    }}
  >
    <Container>
      <Row>
        <Col>
          <div
            className="text-center text-light p-4"
            style={{
              fontWeight: 'bold',
              backgroundColor: 'rgba(0,0,0,0.5)',
              borderRadius: '20px',
            }}
          >
            <h1
              className="mb-5"
              style={{
                fontFamily: 'Segoe UI',
                fontSize: '5rem',
                backgroundImage:
                  'linear-gradient(to right, #51e263, #048b4c, #1cbb63,#30cc5f, #2ec05b, #0b9f54, #0cb460, #3cc45c)',
                backgroundClip: 'text',
                WebkitBackgroundClip: 'text',
                WebkitTextFillColor: 'transparent',
                textFillColor: 'transparent',
              }}
            >
              NutriNet
            </h1>
            <h2
              className="h4 font-italic"
              style={{ fontFamily: 'Segoe UI', fontSize: '2rem' }}
            >
              NutriNet connects users with{' '}
              <span style={{ color: '#51e263' }}>ingredients</span>,{' '}
              <span style={{ color: '#30cc5f' }}>recipes</span>, and{' '}
              <span style={{ color: '#3cc45c' }}>restaurants</span> to help
              them eat healthier and live better.
            </h2>
            <div className="mt-5">
                <Link to="/ingredients">
                    <Button variant="outline-light" className="mr-2" style={{ padding: '10px 20px', marginRight: '40px' }}>
                    Ingredients
                    </Button>
                </Link>
                <Link to="/recipes">
                    <Button variant="outline-light" className="mr-2" style={{ padding: '10px 20px', marginRight: '40px' }}>
                    Recipes
                    </Button>
                </Link>
                <Link to="/restaurants">
                    <Button variant="outline-light" style={{ padding: '10px 20px' }}>
                    Restaurants
                    </Button>
                </Link>
                </div>
          </div>
        </Col>
      </Row>
    </Container>
  </div>
);

export default Splash;