import React, { useEffect } from 'react';
import { Container, Image, Card, Row, Col, Button } from 'react-bootstrap';
import InfoCards from '../components/InfoCards';
// import { getBranchNames, getBranchCommits, getIssues } from '../utils/gitlabAPI';
import { getBranchCommits, getIssues } from '../utils/gitlabAPI';
import { useState } from 'react';

const About = () => {
  // const [branches, setBranches] = useState([]);
  const [commits, setCommits] = useState([]);
  const [issues, setIssues] = useState([]);
  const unitTests = 32;

  const persons = [
    {
      name: 'Sumit Chakraborty',
      role: 'Back End',
      gitlab_username: "sumitc2",
      gitlab_emails: ["sumitc@utexas.edu", "sumitc@cs.utexas.edu"],
      caption: "I am a sophomore CS and Math major at the University of Texas at Austin. " +
      "In my free time, I like to play soccer and chess.", 
      img_src: require("../pics/sumit.png"),
      commits: 0, 
      issues: 0,
      unit_tests: 6
    },
    {
      name: 'Arjun Hegde',
      role: 'AWS',
      gitlab_username: "arjunhegde",
      gitlab_emails: ["arjunhegde@utexas.edu"], 
      caption: "My name is Arjun Hegde, a Junior CS major and Business minor at the University of Texas "+
      "at Austin, born and raised in Boston, Massachusetts. My hobbies include watching movies, "+
      "playing guitar, and watching the Longhorns/Boston sports teams.", 
      img_src: require("../pics/arjun.png"),
      commits: 0, 
      issues: 0,
      unit_tests: 6
    },
    {
      name: 'Ashton Mehta',
      role: 'Full Stack',
      gitlab_username: "anmehta26",
      gitlab_emails: ["anmehta26@utexas.edu", "anmehta26@gmail.com"], 
      caption: "Ashton is a sophomore computer science major at the University of Texas at Austin. His "+
      "interests encompass full stack development and data analysis. Outside of school, he is "+
       "involved in fusion dance and running.", 
      img_src: require("../pics/ashton.jpg"),
      commits: 0, 
      issues: 0,
      unit_tests: 6
    },
    {
      name: 'Ryan Harding',
      role: "Front End & Dev Ops",
      gitlab_username: "brickgt",
      gitlab_emails: ["brickgt@gmail.com"], 
      caption: "I am a sophomore CS major and business minor student at the University of Texas at Austin. "+
      "Some hobbies of mine include creating music, watching sports, playing video games, and exercising.", 
      img_src: require("../pics/ryan.webp"),
      commits: 0, 
      issues: 0,
      unit_tests: 6
    },
    {
      name: 'Oscar Goes',
      role: "Back End",
      gitlab_username: "goesoscar",
      gitlab_emails: ["goesoscar@utexas.edu", "goesoscar@gmail.com"], 
      caption: "My name is Oscar Goes, I am a sophomore CS major and Business minor at the University of Texas at "+
      "Austin. Some of my hobbies are photography, video games, and mechanical keyboards.", 
      img_src: require("../pics/oscar.jpg"),
      commits: 0, 
      issues: 0,
      unit_tests: 8
    }
  ];

  const sources = [
    {
      name: 'Edamam Food Database API',
      img_src: "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAOEAAADhCAMAAAAJbSJIAAAAkFBMVEVqzAD////c8L9hygBmywDI6677/vXg89OQ2FSr4YJjygBdyQDh8sXe8cLE6Jyp33d/0jfy+uvU78D8/vnu+eWX2mPr+OCF1EPL7LN60S2R2Fn3/PC55Znn9tu45ZfV7rTR7rt0zx6b22qy447a8ciK1kzY8MWe3HLA6KXj8set4Yfj9da55I+v4YHO66vB55lx09d9AAAK8klEQVR4nO2daXvqKhDHY0FbQetaNS6tcam1Z7nf/9tdrcZskDADCPE5/1fnuS+8/AphhplhCBqPrsD1AKzrH2H99Y/QmFrj8er5c7her4fD3WoyXtzrf2ybcDTYzb4P04ARQjiJxU//ZEF/0/4aTkaWR2CRcPT8degHZxZKaSAQpSduTsO35c7ijFoiHKzb/dNEickKpIzwYNOc2BmKBcLRsB2dFyFIlJ4pZwPzwzFNuGi+MChdgkl4/930VBolXDSnp5WJxLtCMh6ZhTRIOJwTTbyrGO83W8aGZYpwsA24EbwfUUIOz4ZGZoZwN+fYb08KycOZkbGZIFz3DU5fSiRYGnAH9AlnkR2+H0a21f4gdQlnp7+0TTGy1ZxHPcJhaG/+YhG6dEa42nPbeBfGYO2EsHWwP3+x+BTvBKAJm9S0fSgT5e07E46n91mgiUjweU/CpRn3DCTKN6hdFUM4mNq1EDKxYHcfwub9dpi8eO8OhKPNvb/AtEg4tk04ie65hRZFCdQfBxLOXE7gRdCVCiNsuwc8rdQpyBsHEe7d7KF50QDi4QAIB44/wZTI0AbhSi34eR/xpnnCnR8rNBbfmiZc+7DHpKW8pSoSemAl8iIHk4RN/wBPiG/mCL0EPCFuTBF6uEQvUlqoCoTebTKJiMJ2U0248xdQyWhUEk78soN5VZv+KsKFa4Qq8SoHroow9MhVE4tUuOEVhHNvnO0SlR+mygm3fn+EF9EpnnDo8zaaqNxmlBEO6gF42m3KYjdlhP7vMrFISQSuhLBXh4/wIhpiCGvyEV5U8ilKCVu1WaI/4tKAv5RwXi/CIJClbWSE3p6YZGKyw6KEsGZr9CwuyS9KCN/q4K1lRSUo4v/s9ZlQJiJOhIsJI8gvU2JBmK+EC08ZQsJ3gK2nfD4ctwxr3OwjVpHYBRcRLiCAgakawpyaCI+Ki+puRIQHyDZjrQYdlUZQI5wAFgi3NINnLeGIRFAgJiAEeDN0bg/wNDa4aNGzKRI+A6aQ6BScVeobbpRJMbpYJJwCNmoOroyAaI34EklhEguEkCkMuNUrPZi9pjiJBcIXiK3lFm6AJMLMYcDyf/Q84QpkaSH5dLi2GOe4sJ3mCd9A7hJVS+EhBfIdE5UTDoALg6zsAWK8mvOQcoG3HCF4YUTW9poV9gCXi0rlCMEuPYWX0qlpx7CH8JyflSVE7F6U936Zu6R0VWs3x59QaTaekSWEWPubmPmCYb2fJC0p4biOR/uiSFNKiDJA/on2pYRR/SJsQmXCGWlCkEvqs9i7hLD9GIv0pEhC6Hpc5pRepinCX4+ySLPLNEX4IDvpWendNEXYdz0ug0odXBNCdNb+3N1CJrRzqamU0U8IZ7jDCgvefr/K9N+fqZuln/JNE8IN6s9NNk+dbomOfx3VOwgIUb9D/hyfytXt9l0gJvbiRjjGLFI6rQI8IX64IEw+xBsh6jMk/3UrCZ+OoPCdISUf4o2whwpsfVQDPnVQP62pJCN8+wfKGioSOvkQF3lClDVkv1VWKSpyoCuyyxEindKwUwnYfXVSPcaWOUJkcJLNjxWz2PkwPHRF3YLVMSEik/Uj1v977JSo+9tV3CDMEe6xA6Ek3PRkOswDZweWOFkaE+qEaChlErm8shh7NVfC1uOcDWPFebErIaQ6oSYiXxnCz/rUA6uKtTOEyMOhz4o90yvhA8VoYsWxmivh44RKU8oQwpLb9RDLEDo5h1sWWaQJkUUBsahhmSEcpwl16Ajtv5jVvk8M7O1Xp+ZKiN9oWDCzUKywMNCJiq9ShCM0oeKleASjdhTyega+ELawi4LuLQGexhRoIpLPFCGk7jkjZrFNte6FgavrrUfI0J3wVKRpwq7Fr3qEBNNATVlLPUfLCCG32h1/qGcyMoTYnca/Cto0Yfo7HGEJLXWKv0jzSJfZS7EWn6i3a0LooLnTPBsgzFYfGRbeDbko49Og/VKbkwi6uiMaW8YvRZ8t7F2aWeoGx7JnC3zyRHQRx4BGB+3o37UI80qIS+JffijaDp8Na90z0KU4e8bXidOUVZvc84JlfljZOM3740Wi4vuWV0LU9RS/RQ8ZQs+6IpoQ+84Q1qZRi7rimyVXwtHjRRPjk12cPwxdD8i44vLEmLB2jT6qlcsBP5y5uB0KYsKHMxexsbgR4pPAlDBpHv+cyjc5bIDiFHBSE4WdQ/Ly+nGUqfPx7aqB9O3QcyPElpu8lpYMdbudjZv1fwsh3QhxWWD2t7Ls67hxMou36MONEBW6o5vqCtqnrvHRKyiJVd8IUSFT8lehNrHjIsGcdHtI6rwxgQxSzeeogpYMioSYQ7DaHGoEENCKGkVCzIfIetX1pU8dB9caUymjhBAX2f+onMTjHwfmItXsIXXvCWURo4/yWewev13Yw1R3jBThF2ootPdRVkH76uTVD/rSEBEib3JTEvVlCqmDp6+CbCg+fYfU/CnYldudzmumCRGtp/xUpnFbmhB19clHZfJFmfv4j1LdRhYyQtxu6p2yPdYyhOjCIb+U7e2U7W3iwoO0oIacsJZ9S/Ni2xJC3TpTL5RrsZYjRF3wMp9A1InQ5Xs55ggR+QtG5sumYb1P8e9k5isL8t3NwAEpfjDeJeqsCdZjLxTA5Amh4RrAE29AIdv6F/rQFvomwoIqgm6axoRLFkX5nykQwnKlhd8zKGj/vx8Vnykt9i+FlCLdkgNWhIhCCtqWF/8L5E/HLbZNRJkuwWslgj7CgLCid/Wlgpc8BIQA/5tbsRSx4JciRc9biTpgq5/1icVm14jDnLApu7DrvHJ8JdMYzbjAHUOE24KQEPDEjEVAcFqaCZ9DFA9R+a8nfRnEgMAxFSrcFcSE6maff9sCfAF7yGIHUrLM1N9/IFMru80QnP+XPb8m+5DUb45REh3ahrUJEK8GSNr9ygghfb3lXSOwQvjc0mps6WZYi4cBE8mfCJRv9/V5V+4sef97OWGtYvwlB/ESk12jZ7vKzHKZU+Km1Aejss7wpW5XXTonl16hKyUc12MSSx/pLCesxyuPktfI1Agb7/4jsooL81XHn4PvNoNGFQSVB7y9598irXrqpvoI6/eGWh3tqyZcOKsZUZD8gVUAYWPgL6HwsTw4ob9msdwQAggbEz9nUS3vpRYsm/g4i4qJPcVw4Nhlj0exlJaoOmFj4NuOqrLJgAgbi9CrlareNwYQtHZSCysWDdRbjkDC8vpNAAyJhYC+OKDEgycnDQJ6/RSWWvl0dtkuJQ6rjgAmjwauLtvdRKGvSoLTYxu3K5WE0IdB4QnAmZvy+4s4vH0aIsU5Dl2ZDcoQ756ikrhbfF2djvgcUxiBS1OvHEwjo4qOqBHCs2m88zTyN2RlC7rUYPByT0YSofsXahRTDBGJWpwY0ahq0SoXWZJ72H/KDzrtGfUKYlpt64yUz/XKA3VLfgYHbpORcu1SD/2ipkHP2jxS/qJfymKibGuxpTb2HEY2JspXzRSmjb4iw7aDEvYN9bHFMlZ6t9sY6SN3wWN8inNgBDJYXNj6MtJKPWA82hp8ztxs+eR4GXK9mWQ8aJstlDNeIDr+2hPk5koJ729/mR6QjRLY1rAdQW9nsdMC38zM7C1Z2SryXQy3e8IVMCllhPOwNzP46WVks4y5MVlv5yHlp/ksvG1FzwWNhHAW7b+bxldmWlYJfzQa7Nbv7c00Clh8T5GxIOq/9bazz4nV6ww/sk+YVqu1WCxaLav3UPK6L6EL/SOsv/4R1l//A0fDx3mMmsxoAAAAAElFTkSuQmCC",
      caption: "Edamam provides access to a food and grocery database with close to 900,000 basic foods, restaurant items and consumer packaged foods. The foods in the Food API can be filtered by Diet and health filters generated by Edamam. All food database data is enriched with diet, allergy and nutrition labeling, as calculated by Edamam based on the food's ingredients. Peanut Free, Shellfish Free, Gluten Free, Vegan, and Vegetarian are some of the 70+ claims generated automatically. For basic foods from the food database (flour, eggs, flour etc.), Edamam returns data for calories, fats, carbohydrates, protein, cholesterol, sodium, etc. for a total of 28 nutrients. For UPC foods and fast foods data is return as listed on their nutrition label",
      link: "https://developer.edamam.com/food-database-api",
    },
    {
      name: 'TheMealDB',
      img_src: "https://www.themealdb.com/images/meal-icon.png",
      caption: "TheMealDB provides rich queries with the ability to parse recipes by name, category, area, ingredients, and more options. Data includes connection to multiple outside sources and a wide variety of foods.",
      link: "https://www.themealdb.com/api.php"
    },
    {
      name: 'Spoonacular API',
      img_src: "http://coffee-cereal.com/wordpress/wp-content/uploads/2016/08/Spoonacular-Logo.png",
      caption: "We automatically analyze recipes to check for ingredients that contain common allergens, such as wheat, dairy, eggs, soy, nuts, etc. We also determine whether a recipe is vegan, vegetarian, Paleo friendly, Whole30 compliant, and more. We compute the nutritional information for recipes automatically using a proprietary algorithm. With this information, you can find individual recipes or even create entire meal plans that satisfy your users' dietary goals.",
      link: 'https://spoonacular.com/food-api'
    },
    {
      name: 'Google Maps API',
      img_src: "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAOEAAADhCAMAAAAJbSJIAAABgFBMVEX///80qFP7vARChfQac+jqQzVfY2hcYGVZXGFTV13a29x8foKMjpGGiItTWF7T1NVPVFrLzM1LUFamp6n7uACen6EAZuYco0Th4eLQ0dLZ2tv4+Pg6gfRChPYppUz8wwCytLa/wMHv7+8AbecAdvItfPNDgv4hpEfpKxXpNDjt9u9vc3dscHSipKbw9P0AauefvPjpOSj1PhUzqkWo1LJakfW+3sWKx5jX69v/+vJJr2P2vbr8xkgKplf93qG8z/aNr/Fckuzj6/uHrPXP3fyowfRqmu3KyOX85+XwJgCmWZHyQCHFUG/whn/bSE/51dNLbdTrTUByZr2ZXZ9touO6U3zUS1s1pWZvvIDsX1Y3oINjacc6mqPzpqGKYas9k8I/jNv1p3Xudm7wcwD//ObuYifrTzL4qxL1mhs2o3XyhCQ5nZQ8lrT+6L8+j9D803zwdyj8y2Dg1ZX7wS+DxJJprEmPrz7tXAy0szHUtyP7wzziuRv925dqunyWsDy7tC/axQAgAAAKcUlEQVR4nO2c+3vbthWGdbEAUCJpkBJj2ZRdkrbjJJKcuIvT5rLEceKsbdpl2a3tbt2ypLtmXZbulq3rv74DkKBIiZQlWTEFPXh/cEyJpM4HnPPhgPKTUkmhUCgUCoVCoVAoFAqFQqFQKBQKhUKhUCgUCoVCoVAoFLPi7u/vu0UH8ZbYv3d0f+VCyMr9o3vHRQc0X46PVi5c3d5eEWxvX72w/eBh0WHNjZPqhYG4mN3ObufaftGxzYOT7auj8oAq0Nk9kr4qH65k6+MCucaTokM8G48uZOsTAoHdqsSmc7ydUX9DAqvVzR1pp/Fe3gSudKopdh8VHepsHE0qEKrxsOhgZ+FBrsDdYYFySsyfwZVRgTJK/N50AkGiZLX4wYcf7UwlEOzmWtFBT0Wt9vjjbImbeQqrOzL1qZ8c1GoH35/IRhNsFh325Dy5VAMOaj8YmcZxAqudo6IDn5intZDHPxyWOE4g5Kks/dvtg0hh7d0f7UwhsNr5cdGhT0htwOOf7EwucLO69n7RsU/E7UtJiT9NCuzsMjo5ftr5dO+zooOfiM9rSQ6eCr/p7B6ePDze3394cn8ny3Eu/myvvHal6Ogn4INLtTSPf84lbj8a+Mj+g51Rgb/4Trm8d73AyCfllwdDCmvv/gokbqfX8+Pq0DR2vgCB5fKtgqKehs+HBYLEj3Y6I8+cDlMSN3/NBcqQpu5wkvJE/TjjgdNhwnA2D7e4wPLejfMPeUpujyQpcOlJxpn7iX3i5qeRwq33zj3iaRktQ/DT32SeehLnKbPRiMUvxGejCi//9nn2uUIht9GItfMNdwaejk7hizs5514LJV78YiBQAqsZNZovK3dzzj3eTdpopHDRG7cMK221cpK0VNrlNpoUuPhmOtLRXP5dpXUz72w+hbfKUiu8/Pt3Kq3cs2FJvPiHLakVXv7jO5Uxc3hYvfinVI7Kp/AFCKyMqcOLfx4SuPgK007zJRNYab3MOfl45y/DAhdfYXq1qIR8lXPuyeGIwMVfLZIrPthoSJ7VHI7ok2HFf5ay0Uhh9pL/cMhGQ4XnHO/0xJ03t9GxkzhotxMsfuctdk+X/zoQWKlkdabvrWUI3Pr63COeFrFcvEgKrLRGzeZGlkApHtQcRO12mtarodP+lilQAisVG8RWZYTkqvj8TVYNsiwtLO7JYYUYrxOpaXx9N2zfbr6808rWJ0MZ8q5msE4MaWxV3ty5A/+0/p6jcG3hOxrGs+Q6kc0/vpujcK/o4CfiyT9PE/ivPIESPGnjnKKv8u88gRK0bCEvM4w0wX9yBW7J8dVT6bRJzNMnzxSeMol5NipPFTLe5AvMtVEZthUDnudOYq6NyrIWCr7KkZhvo+Wtxd83pcgWmG+jMtlMSHae5uuTLEcZ/82QOEbg1jdFBzw9d0YE5q8Tcuyahrk5LPB/44pQgo3vKEOl+O04gRI8u8jiblLimHVCyiIMeTUQ+HqMwHK56EBn5/UkNirdSpjkpsjTcTYqp8sIIrcZ025L6zIC7jZj2m2Jdr15vBpvozK7jGBsuy21ywiej34LmhAoXb+dRfZXMJw9iZ5bjOPrnO8opNv05nMrR+EyFGHIlZxv0paiCEOuZ37dK/1KmORWxt8kyLjpzScjT5cpRxnXh/10uXKUMeyna9L/dxHDvJ/OUxn+5mJaPkubTdHhvAVSZrNsNhOSmsSig3krJCpxGauQMbBTmb4pnIYbYk2U4u+CZkKkqdxP18YhvGa5OtIkUZrK9BcJ07K25Ekq0lSOP16bDf5QapmTtFQqby3T05ksrnyzdmuJq1ChUCgUCoVCoVAoFMuB2663l+5bvwFBE1FN0+yuN+P1nufPNaB549gEIYwxwqZpzXKDDdtszDuoOeIigjBFht4wTYzs/gy3qGs4R6FlBakTrdUZbn9GXJg7qrf576s90wxOOT+LfIU26SU/i2oFzHUXBK7HR84sAsco1BBJVKiOkTHL/c+EryG6cdabjFOISHzQpqgAhRiRWSovzTiFGMf3b6AC5nDVRDTvvcCyVpNLZNuy6qlrY9sYKIRrUhmh4SahUeZbJhycu8ImwXr2O/UuNU3T1oVGn8CxZnrifcfWTI36TdtejxW22TWalqg8jbR1oQoRz4t+9/VeVw8XJsurlywDI12MjG90G/2Z7CATAxMRc9AWsNt7NobVkSBihjZraGy1xMjs8kO2xBA4NhEyV4XC9fAaTAeDppH1gGo8eI+gUjSHXQLXEtJlo2dQX6caHFKHDxKGtzRCvdKc6OJ4jXdsM8RusmAR6frrHoGYWRwOZLNuWQ5FYYw9jLTmutVIKgzYNdaGB+Ycz6JGrFKfrxiuBp8UKuwSs1mvw73ZaIG/mkY9gKk24XPBF3pWe1Un9rwWzgY2RTSOiTmIONyA+JCCFPZL246WlDpFFD7bMxHmidQ0Bwp1HGZ8oA1qmyksEbZi6KRRihT6iIffNjX4V0c4/CSPFWxbI/y+TnNeeapj0ox+9Qyd0WCvrGo4WqnB4SFchwjH9XjhwsxFa2gPxwptZMILLswhtr2kQp8QuA9tC4Xxh7PPhnGJDrtwCArPvHal8aG00q94bMThh4ixi7U26NDCciwFkKfshxmfLhS2NZiMDd0mhDbiFoIrLPVI0yBsfiOFgYMpxY7O0kOPkoXdCoapQYhjzc9neO1o6UEDRXWYs7g8dTZJBFHhqRTZkF+4Gx1aplC4buJGj4JPJBMsVLihQdm6sUKfEq3XQBRjrtATtyJssB2wHQ3357eTgyRJTWKdT0+TxOXZYIoREouay7KWNSfR236scAPcltBuenMSKiwZKNTBFQY2NpiAoBHOoSgTK+xhAx8GleC5SWQGmOhHXKgwjyevKBiYM5dNZKSYzRR7USjW4zp0bfCM9tDtI4WBHfZuXCGUcnQtVxi3Q/1BkdZRnLtnx4fC6onANnA4pa7wTshXFoEF3hkOKuJaYebDuNiUC6dpiLW1PrCTSGEpcAcK+9GQBibPUhSVuMsMIPCc6MQ5bkI8GHtq+PX2ht+gCGtiFaCeWwp0E9ksgB4oB8kb8C+bAD7zbd63DxTWoaahGXGhzKgYMqEwgisEa2WlHyAUKkSYnR30WOZbtsYvaOB5tncWhS0wNFuQ/NCpRNkHazmhFBwiNH4Yb2xSCj/D4C0YFjiGhiDR0zQpuwbuNtjxZykE3zKNvqERI8xS3dSMvg6LKLszLPyG5/UwnetW2XUoqMPQL9Fm/KJjs5dMLYow6DI5hPaiEVglrEHQGg6rww2bcm/12RBgknhOYNsphY7NtAfIZF1bvWmHXlonMLomCue9SaH7NbW5P/ex+rqhN9eTLwWebjiJD6o3DaOfGFkfrmB9GKw27vpquOtwfcfQvcRqEQQpT3Sjw1XPs1gnHEReanlefOfA528uDDqmw/455Q1I8/STzp/AE12bFnc3M7KgCn0bmzwvG/isC9eCKoT2EdFGX4eulp6x+dC1xVRYakBXycxVO1sVgncai/q43OrasD+YY4e8iATLLU+hUCgUCoVCoVAoFAqFQqFQKBQKhUKhUCgUCoVCoVAozpf/A1we/eu+g//qAAAAAElFTkSuQmCC",
      caption: "Create real-world, real-time experiences with the latest Maps, Routes, and Places features from Google Maps Platform. Built by the Google team for developers everywhere.",
      link: 'https://developers.google.com/maps'
    },
    {
      name: 'Youtube API',
      img_src: "https://upload.wikimedia.org/wikipedia/commons/thumb/0/09/YouTube_full-color_icon_%282017%29.svg/1024px-YouTube_full-color_icon_%282017%29.svg.png",
      caption: "Youtube API is a service which offers developers the capability to query videos, interact with channels, and view other real-time statistics about the world's largest video platform.",
      link: 'https://developers.google.com/youtube/v3'
    }
  ]

  const tools = [
    {
      name: "React",
      img_src: "https://upload.wikimedia.org/wikipedia/commons/thumb/a/a7/React-icon.svg/1280px-React-icon.svg.png",
      caption: "React is an open-source, front end, JavaScript library for building user interfaces or UI components. It is maintained by Facebook and a community of individual developers and companies.",
      link: "https://reactjs.org/",
    },
    {
      name: "Axios",
      img_src: "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAOEAAADhCAMAAAAJbSJIAAAAilBMVEX///9aKeT8+/5UHuNOCeOUe+xTG+Ohi+7j3PpWIeOFZ+pbKuRJAOLr5vtYJeRNDOOplvDNwvbu6vxhM+Xe1/lfL+VxTOfa0vj6+P6LcOuDZOrPxfZlOuaHaurJvfVRFuO8rfOvnvHCtPTm4fp/X+lqQuby7/2mke9uSOe0o/F4Veieh+6SeeyzofEYJMI8AAAD90lEQVR4nO3bbVubMBTGcVMoFU1Daa32Qcd83tz2/b/e1KyOFigB0ivnnOv+v9MXmp+aAAmenSHks/k09AhOXK7PQw/htI21bOH5olCxZOG9Nkqy8OU6U0qy8FuRKsnC0frjL1Sw8GqZKCVZONv9AoUK53aJkSt8yFIlWfi+xCglWXijEiVaOCotMUKF8SEQQm5ByD8I+Qch/yDkH4T8Wj3ufyxOOF2u9z8hTZhnxXj/M8KEM20iycLRRaaUZOH0c0dUsDC3G2pyhbstX7HCzW7LV6hwuvjaMZQpzEt72iKF5VMXicLRpnzqIlA4vd3ftBcn3B5u2ksTzvTh8GUJR09ZZfiihKWroExhHqdVoCTha/VgUJTw4CooTzhd1kxBScI8q/0LlSOc1U9BOcKmKShF2DwFhQiPTEEZwmNTUISw5kZUlPDwWVCccFt7IypIeN82BZkLPw8lJAvnDlOQtXCrHaYgZ2HrVZC5sPFZUIrQ5SrIWphnrlOQqbDDFGQpdL0KshW2PAvyF7Y9C7IXdp2C3ISjJ522Vv0R8BGu1k8Xrd09V4h8hG69VlYiacIJhBCSD0II6QchhPSDEEL6QQgh/SCEkH4QQkg/CCGkH4QQ0g9CCOkHIYT0gxBC+kEIIf0ghJB+EEJIPwghpB+EENIPQgjpByGE9IMQQvpBCCH9IISQfhBCSD8Ia4WZi3B0kvF2r5fQXLam9CQMqFIvoTJtpfohjKdaP2FbaXQVhlPTSYTJ80sYTV2nEGZ3YSz1nUCoX8NQGvIuNPpbGElTvoVpRO1uwLOw+LEK42jOrzBe13+XkPkUGn0fBnE0j0IT52EMx/MnjMxj/bcInDdh8UZujbH5EmqCa4zNj9Don2GG75AXoYm3YUbvkg9htJyGGbxTHoRk1xjbcKEe139lKg0VUl5jbAOFaXwTZtzuDRNGi3mYYXdokDB7o7IpeqQhQv0rzJi71V9o9PcgI+5ab2GakF9jbH2FyS39NcbWU5htGKwxtn5CMscuDvURclljbD2EbNYYW3dhwuA+plxnYbYJM9DedRVyWmNs3YSGztGuc52EaUHnaNe5LkJSR7vOdRDSOtp1zl2oZ2FGODRXoYmJHe065yhME2pHu865CYtr0luiR3MSUjzadc5BqEke7TrnIiR87OJQu3DFdo2xtQu5ByH/IOQfhPyDkH8Q8q9G+Dv0mPxWFXLdj2nqUMjrXMmlA6HJeD8M1rQvjBaU38Hr156Q+Dt4/SoLqb+D16//Qvrv4PXrS2iYbzg1thPSfs93SP+EnDe1W7JCmWuM7UNo9J/Qwzhh70KB9zHlJkl0SfP/lXw10XLXGNuY88GZUwxfH0FU+wsBak8/lwKTzwAAAABJRU5ErkJggg==",
      caption: "Axios is a promise-based HTTP Client for node.js and the browser. It is isomorphic (= it can run in the browser and nodejs with the same codebase). On the server-side it uses the native node.js http module, while on the client (browser) it uses XMLHttpRequests.",
      link: "https://axios-http.com/docs/intro"
    },
    {
      name: "Namecheap",
      img_src: "https://static-00.iconduck.com/assets.00/namecheap-icon-512x512-jcdkwntk.png",
      caption: "Namecheap is a domain name provider for websites and online platforms. They also offer dynamic hosting services for web applications.",
      link: "https://www.namecheap.com/"
    },
    {
      name: "React-Bootstrap",
      img_src: "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAOYAAADbCAMAAABOUB36AAAAV1BMVEX///9B4P1L4f0v3v1x5v3T9v5Y4/3y/P+g7f4p3v3p+/+z8f74/v/8//9l5f2P6/7B8/7d+f/J9f7r+/+K6v6q7/7L9f6y8f556P2b7f7Z+P+L6v6A6f7T1jhTAAAN1UlEQVR4nN1d24LqKgzdFq2inVrH0VHH///OY73WsAIB0qpnPe5tO6wCIeT6718+5s1meRzX9fi432zXCi98Q3zvRtaY0QXGGDvaLeavHpQyqs3oxrADY4v94dVDU8QCkbwz/Z/M6bTmSF6ZjievHqICDj6OF9iyefUoc/FtwzRPUzr6ffVAs7AVsTwT/eAZXUlZtkTLT92jlZxkC7v7TKVhHEfzNKObVw85AYuIJXvjWX7cMfrlPS9ZorNXjzsS+ySapwldvXrkMajSWLZEF68eewRmkObpchKmb8bVq0cvBhi+Lf42i9/ZT2kDXE3xKQu3cYiY+jH2ybLwM7UfsnCdM9Mcn3+wWnqJ2p/XjDsOU4dC6f6oKTxETT0dftixcNashef+d+mb0fffoEc6N0vmh9+eGTXbQcecAGcyea38l9+j7y6InBvY2PPjaskqv/a9Vb8FmSDjvzSv2C1q3lrgLinNkFoz4ybU+NbBq1GTwdbBJ+YemVvkoDzb+Re92PkLMiWSLcZOqALOqvR4pmz9npIRm2/JUwJbZy5Z+6PJ9EC3pswmUI2TL29i2ELPEPNNaX4JH+xz4d4HYzZK97xfOiviJ7f9z6eeUZjaR8KC9o55gYemTFTlXvBH3noMP3JH5XctqRFVmFCiuLN6O8bPABtURY8kd+pYo+RmEJ75eiRRgqKN6a6FpReeuXpkSd4X69ZLNPFG89zp0oy8Ng6zaEfRQkOXZjMUy5McypK3WTSpptgrTM7FJWdvrodkGaW5OMiQtF+DaEGdsWUs24xzc4BLCkE6zXQtaKCjpDu49IvZD3mVWN8YUMg+eEpviQ6oxUuqbqwGn8tRjjuV+jaB/wQBRp6Uvcsk4ehcUDOtcJtTe2ALMy/Bv6rCpvpq0owkf2DJntQUZjZLEYpR2D1u9ok0k0xeKL6mHQGmKb5drCeLY4BpkUjTMWAKzIZzFFncksmk2eKr8ToYTeKqpbJEomogNgX3H5E0T2h8NFPjP6lSG1aDjmgyz3q1Dk2vETj13kkjD4LvQRvTXkIylWielBaeZ/zLzqD6QehoghvzqoWp0fy343jaRHMmtUeHThRwNt4jT/RocmdTsvefGs8DJzCd/Bb3BaBIkwvWTlXf5/RE8YpaGBB/X0eKNKGeNcqwCdFFC99TzbeL2bFG2px9nLSaNKl6lvc297M9y6DpoZkdS2PYQMXuKtKkiSIJW6TqQU7wwY3g5PenbtPF/Dk4XaeLKk16E74hkSa1m5/0qel2thMo0s7HVaXJ2PNN+EkIKoNO18ZQdGnnjz5p+qo0nXFd/2KiY/dLyAjBPkcqqNJkNmeyuZYR3QLQ658uTTwuYXCECxwDLgE1EOvSxDIokebXNjrV5v4XqX6pS5MJwU+gWTVHNjE1CNd5o0vTMVOl0ayasc0wQwJ1SZcmPlEiaU5CZpcQgDqiSxOrezGSdjpLX6tXoJuMLk0ceCQ/N1e5EzliTIm6NCeYpvDpba3h/YBGhiFoynTaplRx8eDL9xCLVuLM9aaTRICx/w0gaQXX6q3X0ov5tMU6XDB2sQHOzaCRZBUZYddexOrl4lC5uiVnLxpACwqENVeR4XXFz2JyOaHcj8ouHF2aNA/oDE+WzD9vogzG4/uABzkbpy5N6Eb02Ujm0RGhD3OoawDnA5FUaX7B5ecJhE2JOLvFIICyD7xIV6UJj03eLR+oG3N52t1/16fdlePxvanShJEqrEa7DXO09e+U0rlKNJCp6zm4VGnCl3ErKZRgYGy5aW/HG8rn7BlDdiKPj0WTZtSaRb7I7mNmf12Cjh3tbBpwjy6v70GTJrRpYH9Y5Xdw27pz1jqrdoHykf0qpSJNXPEGytmpL07HmOXTPdxZtTU6oHFCcg80oVkPal9TH8liQ/aY4561a/eLBgJW9Gg6H519U8XPpRmBreys2g3YHn4frxrNCVyyFkW5sCFXpoDyChvSnv9OIMxEiyYTEIh2JqcUsDXHgLyhCF1plWiumJEDMYuc5y3snl13uxDLoO1QKS4IDx3l4jPRrt1iIw44//D94WDAnAbN6Y4ZOngNswADV28/S4FNLZ/mmluFowKsQqhCBKtv+cO6BXFkOTS/qvl2VrNuALRhYLSJDRqLGMfp7XOGh5oVNuwtw2RRZT90lkhiDn2eTkkAdm/R0RYFPSFZAo9WCm/6nuD5vmgaWKURTabIj+Txz4tyXfqiCQd/AFtM6EZi5ZwsmL4XmqbGziHXmS32CbJCSFb1oQ+arIB3fgk3MAYnhGSZEfo0zY4LLHWuTzHh/kwxAyjPh6DJf17nphEV5YZHKswp7GHRmvECK+FL+sOoQFt4nxVOZk8iyOzRwqXbKy5ksUJCSJog2tOBYpCfkRYAikzEAWeKSLXokWZ7SXbWE/2F6Ch4AITsi68YPabCORo5pRkbA+/oUPIP1WfGn6m/fH8rNufxTWezvUc+SSIqgiJtFG+5Ny8ouloftSH7/bsU7yhp7+gqC9TvEZfSgM9N4XT2TbMbWO/cNqNSWPH7X6cFESoPQSOqN8uBMx6+Sqel6LhxnVtGxLLt5YZixQhnSzzWlVNsTV5VlW0wkXPfrKupDOv5oZkFYnwfVgwgLKVVaHqxHsRa3Sc/3jm9/w4kecp4eurYZ9iC4l1F0z1vSn2oOyiTVLRuvU6xAWl6I5keNy6Usxu2Rnt8hSNZ4qRm7AE7oZ3dCaMwGAvZA4EON+Hoa9WAGbZ5yUPYMqsvoMzwAuj8mcIXV92YPeyYf4p/wCk0gfKDPpIjSfa2Lk0u2qfzvTnPX45/M1wfU5kmp250zD44SKH1VrMvDSZRBZVjbZpMHnl3HFzYJVvxlVYnARgo9uABHCfwZAFjBQqzcl0d0YENqHzqNBnZ//S5+Xg9uwNE3bggN8grYAxVp8lcJeyTWYiteHH64ZgSBVFeruMooCPo02Sy/p7vhb74y6e4xH8g2LKGAeDeQ0WfJj4x6Nf+84kVU3TrbDtrtoF/xFuARp8ms2rpKPxh7sYeb2vX9aO1n8BVRLxOxB5oYlXIkRGhrprX4Gh3zR6ZgfukUA80mbQ/R7+eh9LCjK2byhnh1VjvHtA+1bYHmvg0R4oKEx3WHbpx7UfXh13VyOPh74Gm03znMga0dVLKWN9up+CKPlAeyhXwDow9JtNxbMLN42RylSk+qq0PmlDf4wqrNZE5Yg9BA2KF2JyXPmhS5/sFnCmkOkZNaLH5vm1BsOS5gfdBE8dL8nfCQ1SS6jV7s01udKUQZyfsgyauZeF7ZxNbBaDNxTUluIeieOyeaGL9wGs2/prllK3oAut8b0LztEX3+SUPWuBlOxzNYHhMNVMhCpftcCJIEgW0iM+Ylw2+lwMlo8jDNqu2zBnIW9EHTZhILq7Ptt7nFl8BN+zBlL2Y0LXvzCl1ddvBVPe4CL3pb5khj1zD0GAXsejK5/NNujxyrmSDXatTumlg5VgC+k0HM5JE9QG7IqPOHunQ0ANNxuMRX989vaN8z1UTPYNLKMbrdOKR1fi8/ra/GphncPU+I6Nn/4HmuJPJ5ujP2Oqgv4qmZzBrVlJkn4CmrVyMXdWq2Y+LMNmntEp1mmvOrRdf1pSqGd1RVYdmdq3Fy/HtmmXUaXKleBNKDtMS2ciatF41v/u2dDQKUXnsE22abG+Z+Fr99FX+3Y0cjg8xpE2TO+oSBC2VZf46FfC4vi8hZZpcKEmKBHLeFajSjyxDNyK6NPl2SDb+ZfQ+F9IWUZeim19Flaan1kjCG+n6D3bYQjEqVzGkSdPTeTelARXN5wjvbrRlLmJIs+eC57xOqJJNg28lahRUTdotrUbTXxUw4RbmdJYQxP/DJmLt31aiOfUbH1NaGtLFwdjTnwFPz6UOzXmzC9huUlrgOa0IRE/BRj5cH7FiJsR+/1eDWnjge0aDyhOhtggDk7mucEaMAMPL21LK9Kc2W0eM+u/x17L8S2CZ3JjSVyisV4iEhwPi85bn7nKRrH2zTGu8Tnvjyt/y+xKeia0pMzodo7aNfSO1M2VO7u7wPYATl6ybiR2jYQzd0TmjdXUOzcHFbZHYwCeT5tCNgNPbrdO9GakWcz3qekFq+558mpKSkm/A0jlQoi/mA4khUvUgFlQ9iDQNOjWne4JN8PR1QSwBkYVa5sOQFGcTs0juKd+CcSWrk6zTRewVqT3lW7AZlqpgs71i4Jjp5I8uh2AJSyLFwzGSSA0tVXQfhzSSyYrPE+j2kpbkOwzA0ZYJdmcM6igV6geh4v8KHIt94qULgYayi06UtUZsI8/QmNHuV5FjC39rXAhWwZOb8DCstaPyOPvOPj9cOK1xQ0oV33jENJMsrOY68gbBEbWBbc8n7SY4V4eD2xrX9+uVZ1cq7yZl0NF6rEprXzJLDxtKE074MafvTT1VLkzZ37bSgRMvhitarZceR5XJvCcNADf6D1SKP3h7WQaKgLwH3Jw6YiecLvxtHhMCBV8A4OYvHgNfbepAV/k3F7F3oLGb3X6zmf3VJhT2b8YJzuOXAEdTydyqNj6u7FVI7ypvig9ZsGewwXGhqUwJA3ghkoytphBWzn4bpLieP20qW/irlaGpLD9pV94RZz03KdGB74AoV6VZvruizgJVlMSwuxzf1Ksh9Ie4daM+DOtwi/JOgaEPRqC3tTH7NzcRCLHlJ9SY+kOlK8ICETXG1ossb/H7YfJT2MfdpDUSl8vmYw8QH+bNZjmu67LeLTfN6i0p/geq4alV/ZwzLQAAAABJRU5ErkJggg==",
      caption: "React-Bootstrap replaces the Bootstrap JavaScript. Each component has been built from scratch as a true React component, without unneeded dependencies like jQuery. As one of the oldest React libraries, React-Bootstrap has evolved and grown alongside React, making it an excellent choice as your UI foundation.",
      link: "https://react-bootstrap.github.io/"
    },
    {
      name: "AWS Amplify",
      img_src: "https://allcode.com/wp-content/uploads/2022/08/FKA4cm2x_400x400.png",
      caption: "AWS Amplify is a complete solution that lets frontend web and mobile developers easily build, ship, and host full-stack applications on AWS, with the flexibility to leverage the breadth of AWS services as use cases evolve. No cloud expertise needed.",
      link: "https://aws.amazon.com/amplify/"
    },
    {
      name: "Postman",
      img_src: "https://res.cloudinary.com/postman/image/upload/t_team_logo/v1629869194/team/2893aede23f01bfcbd2319326bc96a6ed0524eba759745ed6d73405a3a8b67a8",
      caption: "Postman is an API platform for building and using APIs. Postman simplifies each step of the API lifecycle and streamlines collaboration so you can create better APIs—faster.",
      link: "https://www.postman.com/"
    },
    {
      name: "Visual Studio Code",
      img_src: "https://ih1.redbubble.net/image.1470587088.2816/st,small,507x507-pad,600x600,f8f8f8.jpg",
      caption: "Visual Studio Code features a lightning fast source code editor, perfect for day-to-day use. With support for hundreds of languages, VS Code helps you be instantly productive with syntax highlighting, bracket-matching, auto-indentation, box-selection, snippets, and more. Intuitive keyboard shortcuts, easy customization and community-contributed keyboard shortcut mappings let you navigate your code with ease. For serious coding, you'll often benefit from tools with more code understanding than just blocks of text. Visual Studio Code includes built-in support for IntelliSense code completion, rich semantic code understanding and navigation, and code refactoring.",
      link: "https://code.visualstudio.com/"
    },
    {
      name: "GitLab",
      img_src: "https://gitlab.com/uploads/-/system/project/avatar/278964/project_avatar.png",
      caption: "From planning to production, GitLab brings teams together to shorten cycle times, reduce costs, strengthen security, and increase developer productivity.",
      link: "https://about.gitlab.com/"
    },
    {
      name: "Docker",
      img_src: "https://www.underworldcode.org/content/images/size/w600/2020/08/Moby-logo.png",
      caption: "Docker takes away repetitive, mundane configuration tasks and is used throughout the development lifecycle for fast, easy and portable application development – desktop and cloud. Docker’s comprehensive end to end platform includes UIs, CLIs, APIs and security that are engineered to work together across the entire application delivery lifecycle.",
      link: "https://www.docker.com/#"
    },
    {
      name: "MySQL",
      img_src: "https://d1.awsstatic.com/asset-repository/products/amazon-rds/1024px-MySQL.ff87215b43fd7292af172e2a5d9b844217262571.png",
      caption: "MySQL is the world's most popular open source database. With its proven performance, reliability and ease-of-use, MySQL has become the leading database choice for web-based applications, used by high profile web properties including Facebook, Twitter, YouTube, Yahoo! and many more. Oracle drives MySQL innovation, delivering new capabilities to power next generation web, cloud, mobile and embedded applications.",
      link: "https://www.mysql.com/"
    },
    {
      name: "Flask",
      img_src: "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAOEAAADhCAMAAAAJbSJIAAAAh1BMVEX///8AAAD7+/v29vbw8PDv7+/c3Nzq6ur39/fAwMDJycnz8/PCwsK4uLiDg4Pj4+PPz8+Li4ttbW3W1tZ9fX13d3cQEBDd3d03Nzc8PDylpaVVVVWWlpatra1ERERnZ2dNTU0fHx+cnJwsLCwVFRUtLS0kJCRgYGBBQUFTU1OIiIiRkZEaGhoW0LxYAAAPeElEQVR4nO1da3uiOhAGrSAoooA3FO/VtvT//77DJOE+CbhHZXYf3w/ntGrZjJnMfSaa9sYbb7zxxhtvvPHGXwzXGiew+l2v42mY6gJ21yt5EoyEtnjpnv5dEn8FZVZCYtdreQocXd/wn1a67nS7ludgltDl+tOxZR10fdL1ap6BUC9g3/VqnoFBkcKfrlfzFEwKFI67XsxzsM4pHHW9ludgeMwo7Hopz8JoIQj87HolT4Ob7mGv65U8DanKWDmu2WPoekWPhqvXsA67XtRDEdUp/LeU4xQj8F+SrBZO4L8jd2IJgbrb9coeg9FORuA/soeSI8hw6npxj8BYQWDiGptdr+//46qkUNe9adcr/J/4wIi6rUq/f3rh9C/2OY51ChNbZmTvKy8e99FfKnfCOoUr9sZoXnvD73itfwaMTQ3+1rr6+qHLhf45EDblwcVR/Y2o47X+GW7IJjJKMD1idb3aP8EnQogej8oBqgzeoOv13g0ToyPB2Us15aFCo9H1ku8ExqQ5rpNxX1tVXjz8VUaAJ6fu4jmcJetq4/uv0RvOUUbeJlxmn0JUpr75C2jsO7L928VlNsS9jwPxXKNbtcpyDhTkzT6F/zuR+Y+/J8JpcYzxCkTG4w/Qhh/wUaV75VFlVltJYIYr2NqB+jMrmgb5dzsK9V3y2cpLwTZypq7J+LPnMqtgTnAjqxpOCi9KP/rpnb7g/xVHkbP7J7mN3LSlkGEVOO5QEzHHSqJ/mO0trXiHLD5ax3pm59sDGap5+UkFo+9GypyruX4ogvGw9FcgoC7lB5VCrTtCrseskbpDaGq9peWM7YIcCS7rVcm58Kt/RSSeY/02kOdBiVsoNtqTPwjJWFEIk/fU7sQ6Zpt2Yr8sdp6idgE157p3II0Ftq5084Q7odnR2F8O1DrghD7i6wU0KCE3wvbRsvnPczjMQz5MZl5Fu3ZcOIYSuDjMrPvU2VTYclxBDJwi41+fse7WMDACj/c9Y+TMU0bPtUPfyZ2VLu2bHs6fcesH9O24yJPlN6cx+Fn7jwcv+i4UmWkx8zOCA/72+HyVp+5NP/Qqcbl17UPDQccKMV/chEmVSWmpffixIgr7A2PsRDPvXNl2JmY2LyegEWJ5mXWVliXymkuuv7OsqBHNV6hpsDn5Pa3/TZhCRzO9W8DCLGmRiZCkTMMx18//QU/sbuKkRsuALoUudwj4vglCvsUHBnNd3ya6DnEgV0FUdh7OyDnsHHwHucARipnbqLvc1uprEcqbNRFyo0phmlDLVmzY06IGi44of4qsYgFzohQeWXG+XtJlyyhKHcETYrXaUQCKrqpIAoq1mpwyHgnO7JEx2zRwKFw03cS9+qUTVByj4F5j6BWABRtpHErYHmlW4rDHHP/FtzQrOtEXr1p3e/AtSVfPPL962iVHIH0QkB0IxUIKbN1ORsHR+1GW00j9jQDUS5CxASGoyKlDyp/ggh1HnuIr6Ax3xUnlniw7sJcVxfITJHIkwWIij7fkTyHYf6KSKynmoe0qndg8nkyw001Wl1BA80NyUUWx1l2aGL2DwjzWM3v+gu+GupwU0Cw98ma3eeNnX4+hgjaOW/NDQCRPxu0++3qoA96A5rj8khXxbYi2SaGtIyVkeQp5YVAEEUNQGi9Z8p3Aqi0rSFWFojCIGaRfNCnUvhopTPduwSIacmwopGEQ4PkUjMJr4WcMCYV3ZTpeBTSwj3LpVldLy436C+gODfRd8lVDClvxoBtJs01T2qb7yEojhgPbNrThSTUpY0O1qcaR0bfINeGYRzQmI02Rhzjck9N5JZAKdY5cMDLL5wY5QpUwPStz/F1CUhFVdPZY0O2kjT1VtEkn25+AxgwrUpPlmpId6smdXMjN1cLENIBWRC3KxUFDVr++U3nCLPtIrZ6NY4BRmMn9pZA301tZ+NRx0SnGExkuCIVZYK2nb0Q4vGfHC9U0F6jhJFXLlgPTiKKCIvoxknfXGfOpuDDUScaiAKgHtbZtUPc8Vd+qE3+ok+2EUgQVI+FdtXrOmarKV5qmN17128ot+qYZqQHIYt+Q/N2x5gP5+St4xReyKl/SjHAeMZt1sVNZY6M8WQHVKRfpB7vFEqUQdB8UVJq+Qn6csqoG7mm+YLV/BIxAli2EoHhBA5j6LSor9XU+etBtfWA7gGh52hYpZNwHbFrYQVYmVdxRMNWyAgWopSaq8hkhky/dLhjhXOXDC9CtnsoTVrhQMMrZvqWbCFXeNL18zoyboR4UixUZHR7bMyfTAjyAnBc/GUWKQ51mcoYBfHjD2JVC4McBf90a5Sl8IXUz2crYVogX1olAVSGymCJsRTmkMWLesW/nFKbmT3YWWeKf8TAnftfF6tvAFeqhbMB5LII4clIKzTwgkLpRTDixw5cc0b1OV11oR85h/RKFC0aRlu5hrC8zK/0srJxMuiYG7NTWKVYrCLCumQ8hLLOTmLwRx0BVQuFgx15IP5AKUEf87CZWNwgqov6T6Ab5qXjDXGYuD0ChJfhWxDx+s7+c8Toa45D8Z0czD8zB9qZXyZgmfOswDzFINWUkxG3B4J4waqdg65zo2t6CTU9apRZxo31UXvHBxPsuxWvyXwzKooZb37XQ4rwaMT5rS0cuTXS6dpuIV0S1FoxxtSRFaVvvCVs1PJF4rrUinqtKUvmQiGi5AgPfqnEtjTHN/Mfbfq1f1NrApHwQNRbWXmuV6SxM2fPe0HOLiPaV8qh6run8WmwR3hP9vc2NvRPSU845K1Z7ecFKyahuTIHad/e+vRJcyNQypsdpYVJEk4cLJgNdfcFlTfUcFgBRx6ZmtANlw60mZDgW87RqH05q0yk70Q0parIZJ4m68Hnyxgad2SBQDZ1q0QlDta2Q8ya8MwwTT9EEi6dJGeiEQxmyOj7hR8BM71mzOPWa97lLoBQWsvMjVV8JB8hiqjFFTYRdcD7lmDX2UZo62YoFgKwYM0z5bpRFYkZbyYk86xQ7LzJIx0DN+ukHFpGdeIhgoSeO0rZeuwB9qERLhwCKilreVCIqN3hnm61t6sE1pnPIhtzUs7yCQfUDvUSuDCtPYE50kzzqEGh1TQZvWR7EsOvVxUqPxeuIzN7B0DCQ7ViepODU+/B5lwrhixRa1LYXAZ5jydROrYYq8xICXsknA6uEL6WJBQh7GHDQjs3l3yUU/nyaBlgJi1PwIOrBjBQ/2AzCUnhD5Mwp32i2gCS3xBOONa0/rlXClYOkosOBsGEDxnOMN32ljlGlTaPCkVwek82Wajz+bWIl7lnxTLl0umah8pcJuxgQb1tjfW1pSL9YV/w7qSuGWLfAACfsJ/IajDqFqftb6NtHPSX3xnIBhJ19dgj7SIcp35WSoEVlZo+fZsIRG6isuCHjvbkeL88PlPnEgU5xVEYGECUWwqfcHyxnqH4lW/VFmk9ZcB/pVBAF4JU2lA1KI5i4ZCsXeH37HGlP5DxZq5y+YJlRcIaJNihowpGdIlFi7hfVq24vSL5iRlrvgyw8Yr1t/CgiVh1Sabsi7SmyPnukHUNcrVOnECFxQPoosoCGgcyPvDGtiFl19QCjpdMYtYsDztonWujO4m7IUA1E/3m1NjFKgE6EGI/1zwdaDxl1Vtca8KnOR+1KwUISrmQY9qk23FpHhQ0YQHS9YWDQlaxtaIW0hGGaHwI/dMsXvhmfNg9eEMD7nq461TZvTegEt3k+CIfE5XUl/EsDFuPTdgR+yyw0ZhgRmrJfBhy2WHV3ZxEbScbNUexw94AONl86l6CKK86Nc8q7yE6R5pb9pa3jhBNJJ/8WC8/olHcR5Mx3JYDIpb+JzTdNENdp5ExAVdxAMOOnMhgsnR9h4bd/1L1F/jmqSgPip+PKbIJ5uk8+etnXsepRiJNMNKzBvAy3WqoRM5e374dYQ3/igVR0h3iZ4ExsAFigi142DbuwVShxAmXBkmbtzjS7MMFAhXhE8xjCIkosmXcc0YyipjUkrU1UhmtxLG3uitCUN5DFmGnaED90UhTDU7lldCOZ0QB3OGw1EbSEIksa+MtkAE6QU7/U6R4SB3l9J8U8fx8YNJJdYilHKRecF7N8EpxfNxIk3ruLpQxxwTIiOCdkBGYonMWmW77KKF8X9JFrnDO90zgC0uAEtZmxnKPSC+XnKscjVx3GSAS96Cu6Fuqo+hRW7niR0409WNsBfjIbKuAyrAPk+q+CvX6v19h79r7D5p2ZVd3uhlYvtA1s4v4ou/Vqdcdx7MPtoIsnbzw7g0w+tryEFoDPGB6Lndy1bXhL3fAniyjmRDFRr7jHuwKZ0zQQNS2rNkGcYVZx9ux4CHeF55bbPA88hZyv0mDstfEGWjePmTy9LBCfSqSC4kvP483Vm13KKNhSL6iUH93nRambiQoxroMj3cjioX9Juu6+Qlt1i3cpOrLHv4yiNfwiU6j9LcIMStlQeda8nhgvngvl0O1Hwmx3jXCGlfcjFZijqlT+DstzGwpvvTKWFev34rcsS/pudjtI/Xap4z5MIwTFpMLipfEB/z4nA8CFRG9pZup+HwnZv4xOQTXfvDk5zqwUOXl13UP+zS/aRXBYwhtES/HLWYeZXWcob2RKFIWP2oBP3Fgj6zA5nFqpkMC0sV7VjZNd9tbiIdevze0nsjPFn3xl5423jSzDHXwAHno/aL6icNrejMuwi1M+uFliJ8btH+NtLds+YV/Z7wOTeR/Zgi6RiTdpSgGjbgpJ9H2qJ4ywVoStun0axSPnVxS89rBnSfKKGFhUvLxj+3QntWkmdmKfJ0EGvrOtXhotx2OLBp1czkSa33y5iwA7LTUeu3HbrZdRGFpT33Czo9WTDpIv46EUJjRmXLQeaB9tVeX1x0bLdXZBgDXn6JfVwZtP2gijx1NYPE+gqbH7WV+Mh1OY0JgyHBPjeO70dXjOuCObzR3WP7mobs2sT8Gz3OSlUG8x/wecez3JB2ETPtHM6UV8Iw/c4DRezqyH0/P75tyYm5fr7TSR8UNJVcozsJjYr3I+jJmwoS/z0DaiO53JP8N6++KElhvusn/8fj+rHY5eMJmzmeuHsJPG3N549kS9uLFSh8Q1ukyc9+1tfbD9V2zZU9/3p2PLCuNgfmjwC+v4DGnVrZh2OD+UBc7amyQO3SD/8vum608dy4niyU7N1ee9Yj5lpxj61jaoEKpfPjfez+wUOY4Thad4fkCnOaW0JZ7vlFzWsYaeOXXCyW3XWol8Mgfed+mTVsPAt6Pt5La+IsQujp+HII5sg25Hyl3oD0xzmcie8ThxBl3THJGsKnrjjTfeeOONN9544+H4D0pXwVwunEBsAAAAAElFTkSuQmCC",
      caption: "Flask is a web framework in Python which is used for routing, data, and templates. SQLAlchemy is also used in the context of this application.",
      link: "https://flask.palletsprojects.com/en/2.2.x/"
    },
    {
      name: "Jest",
      img_src: "https://camo.githubusercontent.com/f2c80b28082b1568bf6ae3e4b999dcf6916e4f7ef611aa48efed85198ebe53a9/68747470733a2f2f6a6573746a732e696f2f696d672f6a6573742e706e67",
      caption: "Jest is a delightful JavaScript Testing Framework with a focus on simplicity. It works with projects using: Babel, TypeScript, Node, React, Angular, Vue and more!",
      link: "https://jestjs.io/"
    },
    {
      name: "Selenium",
      img_src: "https://upload.wikimedia.org/wikipedia/commons/d/d5/Selenium_Logo.png",
      caption: "Primarily it is for automating web applications for testing purposes, but is certainly not limited to just that. Boring web-based administration tasks can (and should) also be automated as well.",
      link: "https://www.selenium.dev/"
    },
    {
      name: "React-ChartJS-2",
      img_src: "https://react-chartjs-2.js.org/img/logo.svg",
      caption: "React-ChartJS-2 supplies React components for a wide variety of graphs, charts, and other dynamic visualizations to help process and display data in real time.",
      link: "https://react-chartjs-2.js.org/"
    }
  ]

  const AboutNutriNet = () => {
    return (
      <Row className="align-items-center border-bottom pb-4">
        <Col xs={12} md={6} className="my-4">
            <h1>About NutriNet</h1>
            <p>
              NutriNet is a cross-platform, responsive Web application which helps users
              intuitively parse information regarding nutritional content of foods, cooking
              recipes, and availability of certain food items in various restaurants. The main purposes of our application include helping people make more
              informed decisions about what to eat, reducing food waste by matching recipes to
              ingredients already at home, and making it easier for people to create particular
              diets to stay healthy. We hope that NutriNet will offer its users new insight into eating
              food that they enjoy while also connecting them to new and exciting dishes.
            </p>
            <Row>
              <Col>
                <Button variant="outline-info" href="https://gitlab.com/goesoscar/NutriNet">GitLab Repo</Button>
              </Col>
              <Col>
                <Button variant="outline-info" href="https://documenter.getpostman.com/view/25860975/2s93CExwkQ">Postman Docs</Button>
              </Col>
            </Row>
        </Col>
        <Col xs={12} md={6} className="my-4">
          <Image src="https://images.unsplash.com/photo-1533777857889-4be7c70b33f7?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=1740&q=80" fluid  style={{ borderRadius: '50px' }}/>
        </Col>
      </Row>
    );
  }

  const OurTeam = () => {
    return (
      <div>
        <Row className="my-4">
          <h1 align="center">Our Team</h1>
        </Row>
        <Row className="justify-content-center">
          {persons.map((person) => (
            <Col xs={6} md={5} lg={2} className="my-4" key={person.name}>
              <Card className="team-card" style={{borderRadius: '20px', boxShadow: '0 0 20px rgba(0,0,0,.1)', transition: 'box-shadow .3s', ':hover': {boxShadow: '0 0 40px rgba(0,0,0,.2)'}}}>
                <div className="team-card-img-wrapper" style={{overflow: 'hidden', borderRadius: '20px 20px 0 0'}}>
                  <Card.Img variant="top" src={person.img_src} style={{objectFit: 'contain', maxHeight: '200px'}} />
                </div>
                <Card.Body className="text-center" style={{padding: '20px'}}>
                  <Card.Title style={{fontSize: '24px', fontWeight: 'bold', marginBottom: '10px'}}>{person.name}</Card.Title>
                  <Card.Text style={{fontSize: '16px', marginBottom: '20px', fontWeight: 'bold'}}>{person.role}</Card.Text>
                  <Card.Text style={{fontSize: '16px', marginBottom: '20px'}}>{person.caption}</Card.Text>
                  <Container className="mt-3">
                    <Row>
                      <Col>
                        <p className="team-card-stat" style={{ fontSize: '14px', marginBottom: '10px' }}>
                          Commits: {commits.filter(commit => person.gitlab_emails.some(email => email === commit.committer_email)).length}
                        </p>
                      </Col>
                      <Col>
                        <p className="team-card-stat" style={{fontSize: '14px', marginBottom: '10px'}}>Issues: {issues.filter(issue => issue.author.username === person.gitlab_username).length}</p>
                      </Col>
                      <Col>
                        <p className="team-card-stat" style={{fontSize: '14px', marginBottom: '10px'}}>Unit Tests: {person.unit_tests}</p>
                      </Col>
                    </Row>
                  </Container>
                </Card.Body>
              </Card>
            </Col>
          ))}
        </Row>
      </div>
    );
  }
  
  const TotalStats = () => {
    return (
      <div className="my-4 py-4">
        <h1 className="text-center mb-4" style={{ fontSize: '3rem' }}>
          Total Stats
        </h1>
        <Container>
          <Row className="justify-content-center">
            <Col className="text-center" md="3">
              <div className="py-3">
                <h2 className="mb-3" style={{ fontSize: '2rem' }}>
                  {commits.length}
                </h2>
                <p style={{ fontSize: '1.2rem' }}>Commits</p>
              </div>
            </Col>
            <Col className="text-center" md="3">
              <div className="py-3">
                <h2 className="mb-3" style={{ fontSize: '2rem' }}>
                  {issues.length}
                </h2>
                <p style={{ fontSize: '1.2rem' }}>Issues</p>
              </div>
            </Col>
            <Col className="text-center" md="3">
              <div className="py-3">
                <h2 className="mb-3" style={{ fontSize: '2rem' }}>
                  {unitTests}
                </h2>
                <p style={{ fontSize: '1.2rem' }}>Unit Tests</p>
              </div>
            </Col>
          </Row>
        </Container>
      </div>
    );
  };
  
  

  const DataSources = () => {

    return (
      <InfoCards title="APIs and Additional Data Sources" items={sources} />
    );

  }

  const ToolsUsed = () => {
    return (
      <InfoCards title="Tools Used" items={tools} />
    );
  }

  useEffect(() => {
    // const fetchData = async () => {
    //   const result = await getBranchNames();
    //   setBranches(result);
    // };

    const fetchIssues = async () => {
      const result = await getIssues();
      setIssues(result);
    };

    // fetchData();
    fetchIssues();
  }, []);

  useEffect(() => {
    const fetchData = async () => {
      const result = await getBranchCommits();
      setCommits(result);
    };
    fetchData();
  }, []);



  return (
    <Container fluid>
      <AboutNutriNet />
      <TotalStats />
      <OurTeam />
      <DataSources />
      <ToolsUsed />
    </Container>

  )

};

export default About;
