import React, { useState, useEffect } from 'react';
import { getIngredients } from '../utils/nutriRestAPI';
import IngredientsCard from '../components/cards/IngredientsCard';
import Search from '../components/Search';
import SortDropdown from '../components/SortDropdown';
import PaginationComponent from '../components/PaginationComponent';

const Ingredients = () => {
  const TOTAL_INGREDIENTS = 1000;
  const sortOptions = ['', 'name', 'sodium', 'calories', 'protein', 'cholesterol', 'fat', 'weight'];
  const [ingredients, setIngredients] = useState([]);
  const [currentPage, setCurrentPage] = useState(1);
  const [search, setSearch] = useState('');
  const [sort, setSort] = useState('');
  const [itemsPerPage, setItemsPerPage] = useState(100);
  const [pageNumbers, setPageNumbers] = useState(Array.from({ length: Math.ceil(TOTAL_INGREDIENTS / itemsPerPage) }, (_, i) => i + 1));

  const handlePageChange = (pageNum) => {
    setCurrentPage(pageNum);
  };

  const handleItemsPerPageChange = (e) => {
    const value = parseInt(e);
    setItemsPerPage(value);
    setPageNumbers(Array.from({ length: Math.ceil(TOTAL_INGREDIENTS / value) }, (_, i) => i + 1));
    setCurrentPage(1);
  };

  useEffect(() => {
    const fetchData = async () => {
      const result = await getIngredients(search, sort, currentPage, itemsPerPage);
      setIngredients(result.data);
    };
    fetchData();
  }, [search, sort, currentPage, itemsPerPage]);

  useEffect(() => {
    setPageNumbers(
      Array.from(
        { length: Math.ceil(TOTAL_INGREDIENTS / itemsPerPage) },
        (_, i) => i + 1
      )
    );
    setCurrentPage(1);
  }, [itemsPerPage]);

  return (
    <div style={{textAlign: 'center'}}>
      <h2>Ingredients Search</h2>
      <h6>Sampling From {TOTAL_INGREDIENTS} Ingredients</h6>
      <PaginationComponent
        pageNumbers={pageNumbers}
        currentPage={currentPage}
        setCurrentPage={handlePageChange}
        handleItemsPerPageChange={handleItemsPerPageChange}
        formValue={itemsPerPage}
        setFormValue={setItemsPerPage}
      />
      <Search handleSearchChange={setSearch} />
      <SortDropdown options={sortOptions} handleDropdownChange={setSort} />
      <p>*All nutritional information is based on one serving size.*</p>
      <div className="d-flex flex-wrap gap-4" style={{display: 'flex', justifyContent: 'center'}}>
        {ingredients.map((ingredient) => (
          <IngredientsCard
            key={ingredient.id}
            ingredient={ingredient}
            search={search}
          />
        ))}
      </div>
      <PaginationComponent
        pageNumbers={pageNumbers}
        currentPage={currentPage}
        setCurrentPage={handlePageChange}
        handleItemsPerPageChange={handleItemsPerPageChange}
        formValue={itemsPerPage}
        setFormValue={setItemsPerPage}
      />
    </div>
  );
};

export default Ingredients;
