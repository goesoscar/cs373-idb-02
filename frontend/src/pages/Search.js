import React, { useState, useEffect } from 'react';
import { useLocation } from 'react-router-dom';
import queryString from 'query-string';
import { getSearch } from '../utils/nutriRestAPI';
import RestaurantsCard from '../components/cards/RestaurantsCard';
import RecipesCard from '../components/cards/RecipesCard';
import IngredientsCard from '../components/cards/IngredientsCard';

const Search = () => {
  const [currentModel, setCurrentModel] = useState('restaurants');
  const [cards, setCards] = useState([]);
  const [searchTerm, setSearchTerm] = useState('');
  const location = useLocation();

  const fetchData = async (model, term) => {
    const result = await getSearch(model, term);
    if (result && result.data) {
      setCards(result.data);
      console.log(result.data);
    } else {
      setCards([]);
    }
  };

  useEffect(() => {
    const parsedQuery = queryString.parse(location.search);
    const query = parsedQuery.query;

    if (query && query.trim() !== '') {
      setSearchTerm(query);
      fetchData(currentModel, query);
    }
  }, [location, currentModel]);

  const handleModelChange = (e) => {
    setCurrentModel(e.target.value);
  };

  return (
    <div>
      <h1>Search: {searchTerm}</h1>
      <h2>Model: {currentModel}</h2>
        <select
            className="form-select"
            aria-label="Default select example"
            value={currentModel}
            onChange={handleModelChange}
        >
            <option value="restaurants">Restaurants</option>
            <option value="recipes">Recipes</option>
            <option value="ingredients">Ingredients</option>
        </select>

      <div className="d-flex flex-wrap gap-4" style={{display: 'flex', justifyContent: 'center'}}>
        {currentModel === 'restaurants' && cards.map((restaurant) => (
          <RestaurantsCard
            key={restaurant.id}
            restaurant={restaurant}
            search={searchTerm}
          />
        ))}
        {currentModel === 'restaurants' && cards.length === 0 &&
            <h3>No restaurants found</h3>
        }
      </div>
      <div className="d-flex flex-wrap gap-4" style={{display: 'flex', justifyContent: 'center'}}>
        {currentModel === 'recipes' && cards.map((recipe) => (
          <RecipesCard
            key={recipe.id}
            recipe={recipe}
            search={searchTerm}
          />
        ))}
        {currentModel === 'recipes' && cards.length === 0 &&
            <h3>No recipes found</h3>
        }
    </div>
    <div className="d-flex flex-wrap gap-4" style={{display: 'flex', justifyContent: 'center'}}>
        {currentModel === 'ingredients' && cards.map((ingredient) => (
          <IngredientsCard
            key={ingredient.id}
            ingredient={ingredient}
            search={searchTerm}
          />
        ))}
        {currentModel === 'ingredients' && cards.length === 0 &&
            <h3>No ingredients found</h3>
        }
      </div>
    </div>
  );
};

export default Search;