import React, { useState, useEffect } from 'react'
import { useParams } from 'react-router-dom'
import { getRestaurantByID } from '../utils/nutriRestAPI'
import Image from 'react-bootstrap/Image'
import Container from 'react-bootstrap/Container'
import Row from 'react-bootstrap/Row'
import Col from 'react-bootstrap/Col'
import Carousel from 'react-bootstrap/Carousel'
import Button from 'react-bootstrap/Button';
import '../styles/RestaurantCard.css'
import { getIngredients, getRecipes } from '../utils/nutriRestAPI'

const RestaurantDetail = () => {
  const TOTAL_INGREDIENTS = 1000;
  const TOTAL_RECIPES = 421;
  const API_KEY = "AIzaSyCXc_o9v7__6PfXegCEwNR7CUrYaY71u6s";
  const { id } = useParams();
  const [restaurant, setRestaurant] = useState({});
  const [ratingColor, setRatingColor] = useState('black');
  const [similarIngredientID, setSimilarIngredientID] = useState(Math.floor((Math.random() * TOTAL_INGREDIENTS) + 1));
  const [similarRecipeID, setSimilarRecipeID] = useState(Math.floor((Math.random() * TOTAL_RECIPES) + 1));

  useEffect(() => {
    const fetchData = async () => {
      const result = await getRestaurantByID(id);
      setRestaurant(result.data);
      console.log(result.data.address);
    };
    fetchData();
    // new stuff
    const searchTerm = restaurant.cuisines?.join(' ');
    const fetchSimilarIngredient = async () => {
      const result = await getIngredients(searchTerm, '', 1, 1);
      if (result.data.length > 0) {
        setSimilarIngredientID(result.data[0].id);
      }
    }
    fetchSimilarIngredient();

    const fetchSimilarRecipe = async () => {
      const result = await getRecipes(searchTerm, '', 1, 1);
      if (result.data.length > 0) {
        setSimilarRecipeID(result.data[0].id);
      }
    }
    fetchSimilarRecipe();
  }, [id, restaurant.cuisines]);

  const address = `${restaurant.address?.street_addr}, ${restaurant.address?.city}, ${restaurant.address?.state} ${restaurant.address?.zipcode}`;

  const getRatingColor = (num) => {
    let color = 'red';
    if (num >= 4.5) {
      color = 'darkgreen'
    } else if (num >= 4.0) {
      color = 'lightgreen';
    } else if (num >= 3.5) {
      color = '#FFD700';
    } else if (num >= 2.5) {
      color = 'orange';
    }
    return color;
  };
 
  useEffect(() => {
    const ratingColor = getRatingColor(restaurant.rating);
    setRatingColor(ratingColor);
  }, [restaurant.rating]);

  return (
    <Container>
      <Row className="my-4">
        <Col xs={12} md={6} className="d-flex justify-content-center">
          <div class="info">
          <h1>{restaurant.name}</h1>
          <ul className="restaurant-details-ul">
            <li><strong>Type of Food:</strong> {restaurant.cuisines?.join(', ')}</li>
            <li class="rating"><strong>Rating:</strong> {restaurant.rating_count === 0 ? ('No Reviews') : (
              <>
              <strong style={{color: ratingColor}}>{restaurant.rating}</strong> <strong>/ 5.0</strong> based on {restaurant.rating_count} reviews
              </>
            )} </li>
            <li><strong>Price:</strong> {restaurant.pricepoint !== 0 ? '$'.repeat(restaurant.pricepoint) : '$'}</li>
            <li><strong>Phone Number:</strong> +
            {`${restaurant.phone?.slice(0,1)}-${restaurant.phone?.slice(1,4)}
             -${restaurant.phone?.slice(4,7)}-${restaurant.phone?.substring(7)}`}</li>
            <li><strong>Offers Pickup?:</strong> {restaurant.fp_pickup ? 'Yes' : 'No'}</li>
            <li><strong>Offers Delivery?:</strong> {restaurant.fp_delivery || restaurant.tp_delivery ? 'Yes' : 'No'}</li>
          </ul>
          </div>
        </Col>
        <Col xs={12} md={6}>
          <Carousel fade className="carousel" style={{maxHeight: '1000px'}}>
            <Carousel.Item interval={5000}>
              <Image src={restaurant.logo_pic} className="carousel-img" alt="My Image" />
            </Carousel.Item>
            <Carousel.Item interval={5000}>
              <Image src={restaurant.store_pic} className="carousel-img" alt="My Image" />
            </Carousel.Item>
            <Carousel.Item interval={5000}>
              <Image src={restaurant.food_pic} className="carousel-img" alt="My Image" />
            </Carousel.Item>
          </Carousel>
        </Col>
      </Row>
      <Row style={{textAlign: 'center'}}>
        <h5>Address: <strong>{address}</strong></h5>
        {/* Maps embed code taken directly from Google Maps documentation */}
        <iframe
              title="Restaurant Location"
              width="800"
              height="450"
              loading="lazy"
              allowFullScreen
              referrerPolicy="no-referrer-when-downgrade"
              src={`https://www.google.com/maps/embed/v1/place?key=${API_KEY}&q=${restaurant.address?.lat},${restaurant.address?.lon}`}
            ></iframe>
      </Row>
      <Row className="links">
        <Col className="d-flex justify-content-center">
          <Button className="btn-links" href={`https://www.nutrinet.me/ingredients/${similarIngredientID}`}>Similar Ingredient</Button>
        </Col>
        <Col className="d-flex justify-content-center">
        <Button className="btn-links" href={`https://www.nutrinet.me/recipes/${similarRecipeID}`}>Similar Recipe</Button>
        </Col>
      </Row>
    </Container>
  );
};

export default RestaurantDetail;
