import React, { useState, useEffect } from 'react';
import { getRecipes } from '../utils/nutriRestAPI';
import RecipesCard from '../components/cards/RecipesCard';
import Search from '../components/Search';
import SortDropdown from '../components/SortDropdown';
import PaginationComponent from '../components/PaginationComponent';

const Recipes = () => {
  const TOTAL_RECIPES = 421;
  const sortOptions = ['', 'name', 'prep_time', 'servings'];
  const [recipes, setRecipes] = useState([]);
  const [currentPage, setCurrentPage] = useState(1);
  const [search, setSearch] = useState('');
  const [sort, setSort] = useState('');
  const [itemsPerPage, setItemsPerPage] = useState(100);
  const [pageNumbers, setPageNumbers] = useState(Array.from({ length: Math.ceil(TOTAL_RECIPES / itemsPerPage) }, (_, i) => i + 1));
  
  const handlePageChange = (pageNum) => {
    setCurrentPage(pageNum);
  };

  const handleItemsPerPageChange = (e) => {
    const value = parseInt(e);
    setItemsPerPage(value);
    setPageNumbers(Array.from({ length: Math.ceil(TOTAL_RECIPES / value) }, (_, i) => i + 1));
    setCurrentPage(1);
  };

  useEffect(() => {
    const fetchData = async () => {
      const result = await getRecipes(search, sort, currentPage, itemsPerPage);
      setRecipes(result.data);
    };
    fetchData();
  }, [search, sort, currentPage, itemsPerPage ]);

  useEffect(() => {
    setPageNumbers(
      Array.from(
        { length: Math.ceil(TOTAL_RECIPES / itemsPerPage) },
        (_, i) => i + 1
      )
    );
    setCurrentPage(1);
  }, [itemsPerPage]);

  return (
    <div style={{textAlign: 'center'}}>
      <h2>Recipes Search</h2>

      <h6>Sampling From {TOTAL_RECIPES} Recipes</h6>
      <PaginationComponent
        pageNumbers={pageNumbers}
        currentPage={currentPage}
        setCurrentPage={handlePageChange}
        handleItemsPerPageChange={handleItemsPerPageChange}
        formValue={itemsPerPage}
        setFormValue={setItemsPerPage}
      />
      <Search handleSearchChange={setSearch} />
      <SortDropdown options={sortOptions} handleDropdownChange={setSort} />
      <div className="d-flex flex-wrap gap-4" style={{display: 'flex', justifyContent: 'center'}}>
        {recipes.map((recipe) => (
          <RecipesCard
            key={recipe.id}
            recipe={recipe}
            search={search}
          />
        ))}
    </div>
    <PaginationComponent
        pageNumbers={pageNumbers}
        currentPage={currentPage}
        setCurrentPage={handlePageChange}
        handleItemsPerPageChange={handleItemsPerPageChange}
        formValue={itemsPerPage}
        setFormValue={setItemsPerPage}
      />
    </div>
  );
};

export default Recipes;