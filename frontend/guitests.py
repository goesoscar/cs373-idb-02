# Referenced from https://gitlab.com/srenjith/cs373-idb-09/-/blob/part2/front-end/guitests.py
import unittest
import time
from selenium import webdriver
from selenium import common
from selenium.webdriver.chrome.options import Options
from selenium.webdriver.common.by import By
from selenium.webdriver.common.action_chains import ActionChains
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
# from selenium.webdriver.chrome.service import Service
# from webdriver_manager.chrome import ChromeDriverManager

url = "https://www.nutrinet.me/"

class GuiTests(unittest.TestCase):
    def setUp(self):
        # options = webdriver.ChromeOptions()
        # chrome_options = Options()
        # chrome_options.add_argument("--headless")
        # chrome_options.add_argument("--no-sandbox")
        # options.add_argument("--disable-dev-shm-usage")
        # chrome_prefs = {}
        # options.experimental_options["prefs"] = chrome_prefs
        # # Disable images
        # chrome_prefs["profile.default_content_settings"] = {"images": 2}
        # self.driver = webdriver.Chrome(options=options, service=Service(ChromeDriverManager().install()))
        # self.driver.maximize_window()
        # self.driver.get(url)
        chrome_options = Options()
        chrome_options.add_argument("--headless")
        chrome_options.add_argument("--no-sandbox")
        chrome_options.add_argument("--disable-gpu")
        chrome_options.add_argument("--disable-dev-shm-usage")
        chrome_options.add_argument("--window-size=1920,1080")
        self.driver = webdriver.Chrome(options=chrome_options)


    def tearDown(self):
        self.driver.quit()

    def test1(self):
        self.driver.get(url)
        button = self.driver.find_element(By.XPATH, "/html/body/div[1]/nav/div/div/div/a[2]")
        button.click()
        self.assertEqual(self.driver.current_url, url + "about/")


    def test2(self):
        self.driver.get(url + "about/")
        button = self.driver.find_element(By.XPATH, "/html/body/div/nav/div/div/div/a[1]")
        button.click()

        self.assertEqual(self.driver.current_url, url)


    def test3(self):
        self.driver.get(url + "about/")
        button = self.driver.find_element(By.XPATH, "/html/body/div/div/div[1]/div[1]/div/div[1]/a")
        button.click()
        
        self.assertEqual(self.driver.current_url, "https://gitlab.com/goesoscar/NutriNet")

    def test4(self):
        self.driver.get(url)
        button = self.driver.find_element(By.XPATH, "/html/body/div[1]/nav/div/div/div/a[1]")
        button.click()
        nbu = self.driver.find_element(By.XPATH, "/html/body/div[1]/nav/div/div/div/a[2]")
        nbu.click()

        self.assertEqual(self.driver.current_url, url + "about/")

    def test5(self):
        self.driver.get(url + "ingredients/")
        self.driver.implicitly_wait(5)
        
        dropdown = self.driver.find_element(By.XPATH, "/html/body/div[1]/div/div[3]/div/button")
        dropdown.click()
        submenu = self.driver.find_element(By.XPATH, "/html/body/div[1]/div/div[3]/div/div/a[2]")
        submenu.click()
        WebDriverWait(self.driver, 20).until(EC.element_to_be_clickable((By.XPATH, "/html/body/div[1]/div/div[4]/div[1]/div/a")))
        button = self.driver.find_element(By.XPATH, "/html/body/div[1]/div/div[4]/div[1]/div/a")
        button.click()
        
        self.assertEqual(self.driver.current_url, url + "ingredients/1/")

    def test6(self):
        self.driver.get(url + "recipes/")
        self.driver.implicitly_wait(5)
        
        button = self.driver.find_element(By.XPATH, "/html/body/div[1]/nav/div/div/div/a[4]")
        button.click()
        self.driver.implicitly_wait(5)

        WebDriverWait(self.driver, 20).until(EC.element_to_be_clickable((By.XPATH, "/html/body/div[1]/div/div[4]/div[6]/div/a")))
        nbu = self.driver.find_element(By.XPATH, "/html/body/div[1]/div/div[4]/div[6]/div/a")
        nbu.click()

        self.driver.implicitly_wait(5)
        
        self.assertEqual(self.driver.current_url, url + "recipes/6/")

    def test7(self):
        self.driver.get(url + "restaurants/")
        self.driver.implicitly_wait(5)
        
        search_bar = self.driver.find_element(By.XPATH, "/html/body/div[1]/div/div[2]/form/input")
        search_bar.send_keys("Tchiadai")

        WebDriverWait(self.driver, 20).until(EC.element_to_be_clickable((By.XPATH, "/html/body/div[1]/div/div[4]/div/div/a")))
        button = self.driver.find_element(By.XPATH, "/html/body/div[1]/div/div[4]/div/div/a")
        button.click()
        self.assertEqual(self.driver.current_url, url + "restaurants/1/")

    def test8(self):
        self.driver.get(url + "ingredients/")
        self.driver.implicitly_wait(5)
        WebDriverWait(self.driver, 20).until(EC.element_to_be_clickable((By.XPATH, "/html/body/div[1]/div/div[4]/div[1]/div/a")))
        button = self.driver.find_element(By.XPATH, "/html/body/div[1]/div/div[4]/div[1]/div/a")
        button.click()
        self.assertEqual(self.driver.current_url, url + "ingredients/1/")

    def test9(self):
        self.driver.get(url + "recipes/")
        self.driver.implicitly_wait(5)
        WebDriverWait(self.driver, 20).until(EC.element_to_be_clickable((By.XPATH, "/html/body/div[1]/div/div[4]/div[1]/div/a")))
        button = self.driver.find_element(By.XPATH, "/html/body/div[1]/div/div[4]/div[1]/div/a")
        button.click()
        self.assertEqual(self.driver.current_url, url + "recipes/1/")


    def test10(self):
        self.driver.get(url + "restaurants/")
        self.driver.implicitly_wait(5)
        WebDriverWait(self.driver, 20).until(EC.element_to_be_clickable((By.XPATH, "/html/body/div[1]/div/div[4]/div[1]/div/a")))
        button = self.driver.find_element(By.XPATH, "/html/body/div[1]/div/div[4]/div[1]/div/a")
        button.click()
        self.assertEqual(self.driver.current_url, url + "restaurants/1/")

if __name__ == "__main__":
    unittest.main()