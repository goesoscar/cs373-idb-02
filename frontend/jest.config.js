const jsdom = require('jsdom');
const { JSDOM } = jsdom;

const dom = new JSDOM('<!doctype html><html><body></body></html>', {
  url: 'http://localhost/'
});

global.window = dom.window;
global.document = dom.window.document;
global.navigator = {
  userAgent: 'node.js',
};
module.exports = async () => {
    return {
      verbose: true,
      moduleNameMapper: {
        "\\.(css|less|scss|sass)$": "identity-obj-proxy"
      }
      }
    };