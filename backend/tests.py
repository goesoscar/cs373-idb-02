import app
import unittest

# Code design inspired from GeoJobs


class Tests(unittest.TestCase):
    def setUp(self):
        app.app.config["TESTING"] = True
        self.client = app.app.test_client()

    def testGetAllRestaurants(self):
        with self.client:
            response = self.client.get("/restaurants")
            self.assertEqual(response.status_code, 200)
            data = response.json["data"]
            self.assertEqual(len(data), 15)

    def testGetAllIngredients(self):
        with self.client:
            response = self.client.get("/ingredients")
            self.assertEqual(response.status_code, 200)
            data = response.json["data"]
            self.assertEqual(len(data), 15)

    def testGetAllRecipes(self):
        with self.client:
            response = self.client.get("/recipes")
            self.assertEqual(response.status_code, 200)
            data = response.json["data"]
            self.assertEqual(len(data), 15)

    def testGetRestaurantsPagination(self):
        with self.client:
            response = self.client.get("/restaurants?page=1&perPage=20")
            self.assertEqual(response.status_code, 200)
            data = response.json["data"]
            self.assertEqual(len(data), 20)

    def testGetIngredientsPagination(self):
        with self.client:
            response = self.client.get("/ingredients?page=34&perPage=30")
            self.assertEqual(response.status_code, 200)
            data = response.json["data"]
            self.assertEqual(len(data), 10)

    def testGetRecipesPagination(self):
        with self.client:
            response = self.client.get("/recipes?page=22&perPage=20")
            self.assertEqual(response.status_code, 200)
            data = response.json["data"]
            self.assertEqual(len(data), 1)

    def testGetRestaurantInstance(self):
        with self.client:
            response = self.client.get("/restaurants/444")
            self.assertEqual(response.status_code, 200)
            data = (response.json)["data"]
            self.assertEqual(data["name"], "Asiana Indian Cuisine")
            self.assertEqual(data["address"]["city"], "Austin")
            self.assertEqual(data["cuisines"], ["Indian"])
            self.assertEqual(len(data), 15)

    def testGetIngredientInstance(self):
        with self.client:
            response = self.client.get("/ingredients/444")
            self.assertEqual(response.status_code, 200)
            data = (response.json)["data"]
            self.assertEqual(data["name"], "herbs")
            self.assertEqual(data["labels"][2], "VEGAN")
            self.assertEqual(data["calories"], 7.452)
            self.assertEqual(len(data), 10)

    def testGetRecipeInstance(self):
        with self.client:
            response = self.client.get("/recipes/333")
            self.assertEqual(response.status_code, 200)
            data = (response.json)["data"]
            self.assertEqual(data["name"], "World’s Greatest Lasagna Roll Ups")
            self.assertEqual(data["ingredients"][4], "extra virgin olive oil")
            self.assertEqual(data["cuisines"], ["Mediterranean", "Italian", "European"])
            self.assertEqual(len(data), 11)

    def testGetRecipePaginationInstance(self):
        with self.client:
            response = self.client.get("/recipes?page=4&perPage=44")
            self.assertEqual(response.status_code, 200)
            data = (response.json)["data"][0]
            self.assertEqual(data["name"], "Turkey Tomato Cheese Pizza")
            self.assertEqual(data["ingredients"][7], "boneless turkey breast")
            self.assertEqual(data["cuisines"], ["Mediterranean", "Italian", "European"])
            self.assertEqual(len(data), 11)
            self.assertEqual(len(response.json["data"]), 44)
    
    def testSearchRestaurant(self):
        with self.client:
            response = self.client.get("/restaurants?search=slice")
            self.assertEqual(response.status_code, 200)
            data = (response.json)["data"]
            self.assertEqual(len(data), 4)

    def testSearchIngredient(self):
        with self.client:
            response = self.client.get("/ingredients?search=milk")
            self.assertEqual(response.status_code, 200)
            data = (response.json)["data"]
            self.assertEqual(len(data), 15)

    def testSearchRecipe(self):
        with self.client:
            response = self.client.get("/recipes?search=curry")
            self.assertEqual(response.status_code, 200)
            data = (response.json)["data"]
            self.assertEqual(len(data), 8)

    def testSortRestaurants(self):
        with self.client:
            response = self.client.get("/restaurants?sort=rating")
            self.assertEqual(response.status_code, 200)
            data = (response.json)["data"]
            self.assertEqual(data[0]["name"], "Red Lotus Asian Grille")
            self.assertEqual(data[0]["rating"], 5)
            self.assertEqual(len(data), 15)
    
    def testSortIngredients(self):
        with self.client:
            response = self.client.get("/ingredients?sort=protein")
            self.assertEqual(response.status_code, 200)
            data = (response.json)["data"]
            self.assertEqual(data[0]["protein"], 1082.43)
            self.assertEqual(data[0]["name"], "turkey")
            self.assertEqual(len(data), 15)

    def testSortRecipes(self):
        with self.client:
            response = self.client.get("/recipes?sort=servings")
            self.assertEqual(response.status_code, 200)
            data = (response.json)["data"]
            self.assertEqual(data[0]["servings"], 1)
            self.assertEqual(data[0]["name"], "Bacon & Crimini Mushroom Risotto")
            self.assertEqual(len(data), 15)

if __name__ == "__main__":
    unittest.main()
