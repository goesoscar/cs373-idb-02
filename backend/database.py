from models import app, db, Restaurant, Recipe, Ingredient
import json

# Code design inspired from GeoJobs

cuisine_tags = [
    "African",
    "American",
    "British",
    "Cajun",
    "Caribbean",
    "Chinese",
    "Eastern European",
    "European",
    "French",
    "German",
    "Greek",
    "Indian",
    "Irish",
    "Italian",
    "Japanese",
    "Jewish",
    "Korean",
    "Latin American",
    "Mediterranean",
    "Mexican",
    "Middle Eastern",
    "Nordic",
    "Southern",
    "Spanish",
    "Thai",
    "Vietnamese",
]


def populate_database():
    populate_restaurants()
    populate_ingredients()
    populate_recipes()


def populate_restaurants():
    with open("./data/restaurants.json") as jsn:
        data = json.load(jsn)
        for query in data:
            result = query["restaurants"]
            for restaurant in result:
                name = restaurant["name"]
                address = restaurant["address"]
                cuisines = []
                cur_cuisines = restaurant["cuisines"]
                for c in cur_cuisines:
                    if c in cuisine_tags:
                        cuisines.append(c)

                pricepoint = restaurant["dollar_signs"]
                if not pricepoint:
                    pricepoint = 0
                operating_hours = restaurant["local_hours"]["operational"]
                fp_pickup = restaurant["pickup_enabled"]
                fp_delivery = restaurant["delivery_enabled"]
                tp_delivery = restaurant["offers_third_party_delivery"]
                phone = str(restaurant["phone_number"])
                try:
                    rating = restaurant["weighted_rating_value"]
                except KeyError:
                    rating = 0.0
                try:
                    rating_count = restaurant["aggregated_rating_count"]
                except KeyError:
                    rating_count = 0

                try:
                    food_pic = restaurant["food_photos"][0]
                except IndexError:
                    food_pic = "https://cdn-icons-png.flaticon.com/512/737/737967.png"

                try:
                    logo_pic = restaurant["logo_photos"][0]
                except IndexError:
                    logo_pic = "https://cdn-icons-png.flaticon.com/512/948/948036.png"

                try:
                    store_pic = restaurant["store_photos"][0]
                except IndexError:
                    store_pic = "https://cdn-icons-png.flaticon.com/512/869/869636.png"

                db_row = {
                    "name": name,
                    "address": address,
                    "cuisines": cuisines,
                    "pricepoint": pricepoint,
                    "operating_hours": operating_hours,
                    "fp_pickup": fp_pickup,
                    "fp_delivery": fp_delivery,
                    "tp_delivery": tp_delivery,
                    "phone": phone,
                    "rating": rating,
                    "rating_count": rating_count,
                    "food_pic": food_pic,
                    "logo_pic": logo_pic,
                    "store_pic": store_pic,
                }
                db.session.add(Restaurant(**db_row))
        db.session.commit()


def populate_ingredients():
    with open("./data/ingredients.json") as jsn:
        data = json.load(jsn)
        for i in data:
            name = i["ingredient"]
            try:
                sodium = i["nutrients"]["totalNutrients"]["NA"]["quantity"]
            except KeyError:
                sodium = 0.0
            calories = i["nutrients"]["totalNutrients"]["ENERC_KCAL"]["quantity"]
            try:
                protein = i["nutrients"]["totalNutrients"]["PROCNT"]["quantity"]
            except KeyError:
                protein = 0.0
            try:
                cholesterol = i["nutrients"]["totalNutrients"]["CHOLE"]["quantity"]
            except KeyError:
                cholesterol = 0.0
            try:
                fat = i["nutrients"]["totalNutrients"]["FAT"]["quantity"]
            except KeyError:
                fat = 0.0
            labels = i["nutrients"]["healthLabels"]
            image_src = i["nutrients"]["image"]
            weight = i["nutrients"]["totalWeight"]

            db_row = {
                "name": name,
                "sodium": sodium,
                "calories": calories,
                "protein": protein,
                "cholesterol": cholesterol,
                "fat": fat,
                "labels": labels,
                "image_src": image_src,
                "weight": weight,
            }
            db.session.add(Ingredient(**db_row))
        db.session.commit()


def populate_recipes():
    with open("./data/recipes.json") as jsn:
        data = json.load(jsn)
        for r in data:
            name = r["title"]
            prep_time = r["readyInMinutes"]
            cuisines = r["cuisines"]
            diets = r["diets"]
            igr = []
            for i in r["extendedIngredients"]:
                igr.append(i["nameClean"])
            ingredients = igr
            servings = r["servings"]
            summary = r["summary"]
            instructions = r["instructions"]
            img_src = r["image"]
            recipe_src = r["sourceUrl"]

            db_row = {
                "name": name,
                "prep_time": prep_time,
                "cuisines": cuisines,
                "diets": diets,
                "ingredients": ingredients,
                "servings": servings,
                "summary": summary,
                "instructions": instructions,
                "img_src": img_src,
                "recipe_src": recipe_src,
            }
            db.session.add(Recipe(**db_row))
        db.session.commit()


if __name__ == "__main__":
    with app.app_context():
        db.drop_all()
        db.create_all()
        populate_database()
