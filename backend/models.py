from flask import Flask
from flask_sqlalchemy import SQLAlchemy
from flask_cors import CORS

# Code design inspired from GeoJobs

app = Flask(__name__)
CORS(app)
app.debug = True
app.config["SQLALCHEMY_TRACK_MODIFICATIONS"] = False
app.config[
    "SQLALCHEMY_DATABASE_URI"
] = "mysql://admin:12345678@nutri-db.cbfhv8uszc4w.us-east-1.rds.amazonaws.com:3306/nutri-db"
db = SQLAlchemy(app)


class Restaurant(db.Model):
    id = db.Column(db.Integer, primary_key=True, autoincrement=True)
    name = db.Column(db.Text)
    address = db.Column(db.JSON)
    cuisines = db.Column(db.JSON)
    pricepoint = db.Column(db.Integer)
    operating_hours = db.Column(db.JSON)
    fp_pickup = db.Column(db.Boolean)
    fp_delivery = db.Column(db.Boolean)
    tp_delivery = db.Column(db.Boolean)
    phone = db.Column(db.Text)
    rating = db.Column(db.Float)
    rating_count = db.Column(db.Integer)
    food_pic = db.Column(db.Text)
    logo_pic = db.Column(db.Text)
    store_pic = db.Column(db.Text)


class Ingredient(db.Model):
    id = db.Column(db.Integer, primary_key=True, autoincrement=True)
    name = db.Column(db.Text)
    sodium = db.Column(db.Float)
    calories = db.Column(db.Float)
    protein = db.Column(db.Float)
    cholesterol = db.Column(db.Float)
    fat = db.Column(db.Float)
    labels = db.Column(db.JSON)
    image_src = db.Column(db.Text)
    weight = db.Column(db.Float)


class Recipe(db.Model):
    id = db.Column(db.Integer, primary_key=True, autoincrement=True)
    name = db.Column(db.Text)
    prep_time = db.Column(db.Integer)
    cuisines = db.Column(db.JSON)
    diets = db.Column(db.JSON)
    ingredients = db.Column(db.JSON)
    servings = db.Column(db.Integer)
    summary = db.Column(db.Text)
    instructions = db.Column(db.JSON)
    img_src = db.Column(db.Text)
    recipe_src = db.Column(db.Text)
