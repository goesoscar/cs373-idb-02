from flask import jsonify, request
from models import app, db, Restaurant, Ingredient, Recipe
from schema import restaurant_schema, ingredient_schema, recipe_schema
from sqlalchemy.sql import text
import json

DEFAULT_PAGE = 1
DEFAULT_PAGE_SIZE = 15

@app.route("/")
def home():
    return """
    <h1>NutriNet API</h1>
    <h2>Endpoints:</h2>
    <ul>
        <li>
            <h3>Restaurants</h3>
            <h4> Total: 1237 </h4>
            <ul>
                <li>
                    <code>GET /restaurants</code>
                    <ul>
                        <li>Optional query parameters: <code>search</code>, <code>sort</code>, <code>page</code>, <code>perPage</code></li>
                    </ul>
                </li>
                <li>
                    <code>GET /restaurants/&lt;int:id&gt;</code>
                </li>
            </ul>
        </li>
        <li>
            <h3>Ingredients</h3>
            <h4> Total: 1000 </h4>
            <ul>
                <li>
                    <code>GET /ingredients</code>
                    <ul>
                        <li>Optional query parameters: <code>search</code>, <code>sort</code>, <code>page</code>, <code>perPage</code></li>
                    </ul>
                </li>
                <li>
                    <code>GET /ingredients/&lt;int:id&gt;</code>
                </li>
            </ul>
        </li>
        <li>
            <h3>Recipes</h3>
            <h4> Total: 421 </h4>
            <ul>
                <li>
                    <code>GET /recipes</code>
                    <ul>
                        <li>Optional query parameters: <code>search</code>, <code>sort</code>, <code>page</code>, <code>perPage</code></li>
                    </ul>
                </li>
                <li>
                    <code>GET /recipes/&lt;int:id&gt;</code>
                </li>
            </ul>
        </li>
    </ul>
    """

@app.route("/restaurants", methods=["GET"])
def restaurants():
    search = request.args.get("search", "")
    sort = request.args.get("sort", "").lower()
    page = request.args.get("page", DEFAULT_PAGE, type=int)
    per_page = request.args.get("perPage", DEFAULT_PAGE_SIZE, type=int)

    sort_map = {
        "name": Restaurant.name.asc(),
        "pricepoint": Restaurant.pricepoint.asc(),
        "rating": Restaurant.rating.desc(),
        "rating_count": Restaurant.rating_count.desc(),
    }

    if sort:
        order_by_clause = sort_map[sort]

    if search:

        if not sort:
            search_query = text(f"""
                SELECT *
                FROM restaurant
                WHERE MATCH(name, cuisines_text) AGAINST(:search IN NATURAL LANGUAGE MODE);
            """)
        else:
            search_query = text(f"""
                SELECT *
                FROM restaurant
                WHERE MATCH(name, cuisines_text) AGAINST(:search IN NATURAL LANGUAGE MODE)
                ORDER BY {order_by_clause};
            """)

        result = db.session.execute(search_query, {"search": search})
        result_list = [dict(zip(result.keys(), row)) for row in result]
        total = len(result_list)
        sliced_result_list = result_list[(page - 1) * per_page : page * per_page]
         # Convert serialized JSON strings to actual JSON objects/arrays
        for result in sliced_result_list:
            result["address"] = json.loads(result["address"])
            result["cuisines"] = json.loads(result["cuisines"])
            result["operating_hours"] = json.loads(result["operating_hours"])
            result["fp_delivery"] = bool(result["fp_delivery"])
            result["fp_pickup"] = bool(result["fp_pickup"])
            result["tp_delivery"] = bool(result["tp_delivery"])
    else:
        if not sort:
            query = db.session.query(Restaurant)
        else:
            query = db.session.query(Restaurant).order_by(order_by_clause)
        total = query.count()
        sliced_query = query.slice((page - 1) * per_page, page * per_page)
        sliced_result_list = restaurant_schema.dump(sliced_query, many=True)
    
   

    return jsonify({
        "data": sliced_result_list,
        "meta": {
            "count": len(sliced_result_list),
            "page": page,
            "pages": (total + per_page - 1) // per_page,
            "per_page": per_page,
            "total": total,
        },
    })

@app.route("/restaurants/<int:id>")
def get_restaurant(id):
    query = db.session.query(Restaurant).filter_by(id=id)
    try:
        result = restaurant_schema.dump(query, many=True)[0]
    except IndexError:
        return jsonify(f"Invalid restaurant ID: {id}")
    return jsonify({"data": result, "meta": {"count": len(result)}})

@app.route("/ingredients", methods=["GET"])
def ingredients():
    search = request.args.get("search", "")
    sort = request.args.get("sort", "").lower()
    page = request.args.get("page", DEFAULT_PAGE, type=int)
    per_page = request.args.get("perPage", DEFAULT_PAGE_SIZE, type=int)

    sort_map = {
        "name": Ingredient.name.asc(),
        "sodium": Ingredient.sodium.asc(),
        "calories": Ingredient.calories.asc(),
        "protein": Ingredient.protein.desc(),
        "cholesterol": Ingredient.cholesterol.asc(),
        "fat": Ingredient.fat.asc(),
        "weight": Ingredient.weight.asc(),
    }

    if sort:
        order_by_clause = sort_map[sort]

    if search:

        if not sort:
            search_query = text(f"""
                SELECT *
                FROM ingredient
                WHERE MATCH(name, labels_text) AGAINST(:search IN NATURAL LANGUAGE MODE);
            """)
        else:
            search_query = text(f"""
                SELECT *
                FROM ingredient
                WHERE MATCH(name, labels_text) AGAINST(:search IN NATURAL LANGUAGE MODE)
                ORDER BY {order_by_clause};
            """)

        result = db.session.execute(search_query, {"search": search})
        result_list = [dict(zip(result.keys(), row)) for row in result]
        total = len(result_list)
        sliced_result_list = result_list[(page - 1) * per_page : page * per_page]
        for result in sliced_result_list:
            result["labels"] = json.loads(result["labels"])
    else:
        if not sort:
            query = db.session.query(Ingredient)
        else:
            query = db.session.query(Ingredient).order_by(order_by_clause)
        total = query.count()
        sliced_query = query.slice((page - 1) * per_page, page * per_page)
        sliced_result_list = ingredient_schema.dump(sliced_query, many=True)

    return jsonify({
        "data": sliced_result_list,
        "meta": {
            "count": len(sliced_result_list),
            "page": page,
            "pages": (total + per_page - 1) // per_page,
            "per_page": per_page,
            "total": total,
        },
    })

@app.route("/ingredients/<int:id>")
def get_ingredient(id):
    query = db.session.query(Ingredient).filter_by(id=id)
    try:
        result = ingredient_schema.dump(query, many=True)[0]
    except IndexError:
        return jsonify(f"Invalid ingredient ID: {id}")
    return jsonify({"data": result, "meta": {"count": len(result)}})

@app.route("/recipes", methods=["GET"])
def recipes():
    search = request.args.get("search", "")
    sort = request.args.get("sort", "").lower()
    page = request.args.get("page", DEFAULT_PAGE, type=int)
    per_page = request.args.get("perPage", DEFAULT_PAGE_SIZE, type=int)

    sort_map = {
        "name": Recipe.name.asc(),
        "prep_time": Recipe.prep_time.asc(),
        "servings": Recipe.servings.asc(),
    }

    if sort:
        order_by_clause = sort_map[sort]

    if search:

        if not sort:
            search_query = text(f"""
                SELECT *
                FROM recipe
                WHERE MATCH(name, cuisines_text, diets_text, ingredients_text) AGAINST(:search IN NATURAL LANGUAGE MODE);
            """)
        else:
            search_query = text(f"""
                SELECT *
                FROM recipe
                WHERE MATCH(name, cuisines_text, diets_text, ingredients_text) AGAINST(:search IN NATURAL LANGUAGE MODE)
                ORDER BY {order_by_clause};
            """)

        result = db.session.execute(search_query, {"search": search})
        result_list = [dict(zip(result.keys(), row)) for row in result]
        total = len(result_list)
        sliced_result_list = result_list[(page - 1) * per_page : page * per_page]
        for result in sliced_result_list:
            result["cuisines"] = json.loads(result["cuisines"])
            result["diets"] = json.loads(result["diets"])
            result["ingredients"] = json.loads(result["ingredients"])
    else:
        if not sort:
            query = db.session.query(Recipe)
        else:
            query = db.session.query(Recipe).order_by(order_by_clause)
        total = query.count()
        sliced_query = query.slice((page - 1) * per_page, page * per_page)
        sliced_result_list = recipe_schema.dump(sliced_query, many=True)

    return jsonify({
        "data": sliced_result_list,
        "meta": {
            "count": len(sliced_result_list),
            "page": page,
            "pages": (total + per_page - 1) // per_page,
            "per_page": per_page,
            "total": total,
        },
    })


@app.route("/recipes/<int:id>")
def get_recipe(id):
    query = db.session.query(Recipe).filter_by(id=id)
    try:
        result = recipe_schema.dump(query, many=True)[0]
    except IndexError:
        return jsonify(f"Invalid recipe ID: {id}")
    return jsonify({"data": result, "meta": {"count": len(result)}})

if __name__ == "__main__":
    app.run(host="0.0.0.0", port=8080)