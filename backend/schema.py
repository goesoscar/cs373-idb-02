from flask_marshmallow import Marshmallow
from models import Restaurant, Ingredient, Recipe
from marshmallow_sqlalchemy import SQLAlchemyAutoSchema

ma = Marshmallow()


class RestaurantSchema(SQLAlchemyAutoSchema):
    class Meta:
        model = Restaurant


class IngredientSchema(SQLAlchemyAutoSchema):
    class Meta:
        model = Ingredient


class RecipeSchema(SQLAlchemyAutoSchema):
    class Meta:
        model = Recipe


restaurant_schema = RestaurantSchema()
ingredient_schema = IngredientSchema()
recipe_schema = RecipeSchema()
