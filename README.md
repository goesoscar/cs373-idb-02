# NutriNet
## Team Members

| Team Member Name  | GitLab ID    | EID       |
|      ------       |    ------    |  ------   |
| Sumit Chakraborty |  @sumitc2    |  sc65338  |  
| Oscar Goes        |  @goesoscar  |  ojg379   |
| Ryan Harding      |  @brickgt    |  rnh729   |
| Arjun Hegde       |  @arjunhegde |  ah56724  |
| Ashton Mehta      |  @anmehta26  |  anm4883  |

## Git SHAs

| Phase | Leader     | SHA |
| ----- | -----      | ----- |
|   1   | Ryan       | ca8834277816fab89fb0ab648bb8579673e46319   |
|   2   | Ashton     |  a757ec6b7fdcb9625d5878345d81af975401c8c0     |
|   3   |   Oscar         |  5c603c23a024d413fa9b992544763cad5c250f84     |
|   4   |    Sumit        |  6bfc6ef8c0a1eb52383259f9ed2f77613508f363     |

#### Responsibilities of Leader ####

- Create plan for how to accomplish each phase's objectives
- Coordinate with team members to develop optimal solutions
- Organize group meeting times
- Make the submission for the phase

## GitLab Pipelines

https://gitlab.com/goesoscar/NutriNet/-/pipelines

## Website Link

https://www.nutrinet.me

## Completion time
### Phase 1
| Team Member Name | Estimated Time (Hours)| Actual Time (Hours) |
|      ------      |        ------         |        -----        |
| Sumit Chakraborty|          10           |           7         |
| Oscar Goes       |          12           |          10         |
| Ryan Harding     |          10           |          27         |
| Arjun Hegde      |          15           |           8         |
| Ashton Mehta     |          6            |          10         |

##### Comments? ##### 
Web development is very interesting, but JavaScript is not!
### Phase 2
| Team Member Name | Estimated Time (Hours)| Actual Time (Hours) |
|      ------      |        ------         |        -----        |
| Sumit Chakraborty|             21          |          27           |
| Oscar Goes       |            27           |         33            |
| Ryan Harding     |            25           |         35            |
| Arjun Hegde      |             18          |           19          |
| Ashton Mehta     |            20          |          32           |
##### Comments? ##### 
We changed our third model from grocery stores to restaurants. The use case is the same. Code from other groups is so cited in the various files/snippits.
### Phase 3
| Team Member Name | Estimated Time (Hours)| Actual Time (Hours) |
|      ------      |        ------         |        -----        |
| Sumit Chakraborty|            12           |          13           |
| Oscar Goes       |           16            |         15           |
| Ryan Harding     |           20            |         16            |
| Arjun Hegde      |             9         |            11         |
| Ashton Mehta     |              13         |        19             |
##### Comments? ##### 
Some of the "sortable" attributes are still sortable by typing them into the search bar (instead of them being selectable in a dropdown menu). The search bar often searches for more attributes than the name of a card.
### Phase 4
| Team Member Name | Estimated Time (Hours)| Actual Time (Hours) |
|      ------      |        ------         |        -----        |
| Sumit Chakraborty|          6             |         6            |
| Oscar Goes       |           10            |        12             |
| Ryan Harding     |           9            |         11            |
| Arjun Hegde      |           6            |          7           |
| Ashton Mehta     |          11             |         12            |
##### Comments? ##### 
We had a merge conflict right before the submission deadline. We had all of our functionality ready to go in time for turn-in on our dev branch, but the merge to main happened later.
