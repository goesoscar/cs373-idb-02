# IDB Group 02: NutriNet

## Group information

IDB 02 Members
- Oscar Goes
- Sumit Charkraborty
- Arjun Hedge
- Ryan Harding
- Ashton Mehta

## NutriNet: Project Proposal

NutriNet helps people easily parse information related to recipes, ingredients, and grocery stores. With our tools, users can search for recipes based on particular ingredients, view nutritional information about the food that they consume, and efficiently shop for exactly what they want to cook. Filtering based on, for example, a specified amount of carbohydrates or protein, is a great way for people to make sure they maintain a healthy diet while also eating what they love. Having all of this data come together streamlines the meal-planning process and allows consumers to make more informed nutritional decisions.

## APIs

- Ingredients: https://developer.edamam.com/edamam-nutrition-api
- Recipes: https://www.themealdb.com/api.php, https://spoonacular.com/food-api 
- Grocery Stores: https://developer.kroger.com/reference/ 

## Models

**Ingredients and Nutritional Information**
- Instances: ~400 (monthly limit, will investigate multiple accounts)
- Attributes: Name of ingredient, food group classification, amount of: protein, carbs, added sugar, cholesterol, sodium, fat, calories
- Media: Photo of food, fictional nutritional label
- Connection to other models: Popular recipes with this ingredient, where to purchase this ingredient

**Recipes Catalog**
- Instances: ~1,000
- Attributes: Name of recipe, ingredients list, steps to make recipe, recipe nutritional info, type of meal (breakfast, lunch, dinner, etc.)
- Media: Picture of finished product
- Connection to other models: What ingredients does this recipe contain, where can I purchase the ingredients for this recipe, what nutritional information does this recipe have

**Grocery Store Information**
- Instances: ~1,000
- Attributes: Catalog of grocery items, collection of active grocery stores with addresses, price point, delivery options, store rating and reviews
- Media: Map of grocery store, picture of logo of subsidiary grocery store
- Connection to other models: Which stores carry a certain ingredients of recipe, closest stores that have ingredients needed for a recipe

## Organizational technique

We are following the traditional organizational technique of one page per model.

## Questions

1. What kind of recipes can I make with the ingredients that I have?
2. What are some simple, low-calorie or high protein meals?
3. Where can I purchase the necessary ingredients for a particular recipe?
4. How do I create a grocery list which is high in protein?


