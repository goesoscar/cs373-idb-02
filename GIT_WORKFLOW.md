# Git Workflow

1. Start off with issue in the issue board
2. Clone the repo if you havent already and switch to dev branch
    `git checkout dev`
3. Create a branch off of dev with the issue number like so
    `git checkout -b tix-<issue_number> dev`
4. Make changes and commit them as you go
    `git add .`
    `git commit -m "goesoscar/NutriNet#<issue_number> commit message"`
5. Add, Commit, and Push the branch to the repo when you are done
    `git push origin tix-<issue_number>`
6. Switch to and Pull from the dev branch to make sure you are up to date
    `git checkout dev`
    `git pull origin dev`
6. Create a merge request to merge the branch into dev on GitLab
7. Check to make sure pipeline runs successfully and merge the branch into dev. 
    Check the "Delete source branch." option
8. Once branch has been merged into dev, checkout to dev and pull dev to confirm changes
    `git checkout dev`
    `git pull origin dev`
9. Delete the branch on the locally
    `git branch -d tix-<issue_number>`

## More Info
I will be merging dev into main every day or couple days or so.

[More Info](https://docs.gitlab.com/ee/gitlab-basics/feature_branch_workflow.html)