# sourced from https://gitlab.com/anshmor/cs-373-idb-group-3/-/blob/main/Makefile
# as well as https://gitlab.com/forbesye/fitsbits/-/blob/c8bbdbbcee676cb57f7533beb84f47de784289de/makefile
front-update:
	cd frontend/ && npm install

front-start:
	cd frontend/ && npm start

front-build:
	cd frontend/ && npm build

clean:
	rm -f  *.tmp
	rm -rf __pycache__

status:
	make clean
	@echo
	git branch
	git remote -v
	git status

pull:
	make clean
	@echo
	git pull
	git status

format:
	@echo "Formatting backend files using Black"
	black backend